FROM ruby:2.6.3

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir -p /new-ample-samples-api

RUN mkdir -p /usr/local/nvm

WORKDIR   /new-ample-samples-api

COPY Gemfile Gemfile

COPY Gemfile.lock Gemfile.lock

RUN gem install bundler 

RUN bundle install --verbose --jobs 20 --retry 5

COPY . ./

EXPOSE 3000

CMD [ "bundle", "exec", "rails", "server", "-b", "0.0.0.0" ]

