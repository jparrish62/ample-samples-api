require 'rails_helper'

RSpec.describe CurrentUser::CurrentAdminCommand, type: :class do
  describe ".call" do
    context "when a user is authenticated" do
      it "return user associated current user" do
        user   = FactoryBot.create(:user, role: "admin", name: "John Thompson", auth_token: SecureRandom.hex)
        params = { user_id: user.auth_token }
        expect(CurrentUser::CurrentAdminCommand.call(params).name).to eq user.name
      end
    end

    context "when a user is not authenticated" do
      it "returns an error message" do
        params = {user_id: SecureRandom.hex}
        expect { CurrentUser::CurrentAdminCommand.call(params) }.to raise_error(RuntimeError)
      end
    end
  end
end
