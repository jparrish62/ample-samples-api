require 'rails_helper'

RSpec.describe SendDownLoadLink::DownLoadLink, type: :class do
	describe ".down_link_link" do
		it "generates a zip link" do
		   url = "https://res.cloudinary.com/dtfslc8sx/video/upload/v1576622537/02._Chef_Dreds.mp4"
			 expect(described_class.call(url).include? "download").to eq true 
		end
	end
end
