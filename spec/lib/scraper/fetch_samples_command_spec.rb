require 'rails_helper'
RSpec.describe Scraper::FetchSamplesCommand, type: :class do
  describe ".call" do
    context "when params artist, song_title and title are passed in" do
      it "returns searched samples" do
        params = { artist: "Nas", song_title: "It Ain't Hard To Tell", title: "Nas" }
        expect(Scraper::FetchSamplesCommand.call(params)).to be_an_instance_of(Array)
      end
    end
  end
end
