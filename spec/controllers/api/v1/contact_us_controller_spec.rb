require 'rails_helper'

RSpec.describe Api::V1::ContactUsController, type: :controller do

  describe "generate token" do
    before(:each) do
      @email = { name: 'Josh', email: "josh@gmail.com", phone_number: '1234567', message: 'This is an email'}
      post :create, params: {contact_us: @email}, format: :json
    end
    context "when a user does not exist" do
      it "returns a token from rails" do
        expect(json_response[:name]).to eq @email[:name]
      end
    end
  end
end
