require 'rails_helper'

RSpec.describe Api::V1::PendingSamplesController, type: :controller do

  describe "Destroy" do
  #   When a sample is destroyed
  # It find selected sample
  # It destroys sample
  # ---------------------------------------------
    context "when a sample is destroyed" do
      before(:each) do
       user                 = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: "admin")
       @pending_artist      = FactoryBot.create(:pending_artist, new_artist_name: 'nas')
       FactoryBot.create(:artist_datum, user_id: user.id, pending_artist_id: @pending_artist.id)
       pending_sample       = FactoryBot.create(:pending_sample, sampled_artist_name: 'james brown',     pending_artist_id: @pending_artist.id)
       pending_sample_two   = FactoryBot.create(:pending_sample, sampled_artist_name: 'little richie',   pending_artist_id: @pending_artist.id)
       pending_sample_three = FactoryBot.create(:pending_sample, sampled_artist_name: 'Michael Jackson', pending_artist_id: @pending_artist.id)
       post :destroy, params: { user_id: user.auth_token, id: [pending_sample.id] }
     end
      it "removes sample from the db" do
        expect(@pending_artist.pending_samples.length).to eq 2
      end
    end
  #   When multiple samples are destroyed
  # It loops through collection
  # It destroys each sample in the collection
  # -----------------------------------------------------------
  #   When a sample is updated
  # It finds sample
  # It updates that sample
  # -----------------------------------------------------------
    context "when a sample is destroyed" do
      before(:each) do
       user                 = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: "admin")
       @pending_artist      = FactoryBot.create(:pending_artist, new_artist_name: 'nas')
       FactoryBot.create(:artist_datum, user_id: user.id, pending_artist_id: @pending_artist.id)
       pending_sample       = FactoryBot.create(:pending_sample, sampled_artist_name: 'james brown',     pending_artist_id: @pending_artist.id)
       pending_sample_two   = FactoryBot.create(:pending_sample, sampled_artist_name: 'little richie',   pending_artist_id: @pending_artist.id)
       pending_sample_three = FactoryBot.create(:pending_sample, sampled_artist_name: 'Michael Jackson', pending_artist_id: @pending_artist.id)
       delete :destroy, params: { user_id: user.auth_token, id: [pending_sample.id, pending_sample_two.id] }
     end
      it "removes sample from the db" do
        expect(@pending_artist.pending_samples.length).to eq 1
      end
    end
  end
  describe "Update" do

   context "when a sample is updated" do
       before (:each) do
         user             = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: "admin")
         pending_artist   = FactoryBot.create(:pending_artist)
         @pending_sample  = FactoryBot.create(:pending_sample, pending_artist_id: pending_artist.id)
         FactoryBot.create(:artist_datum, user_id: user.id, pending_artist_id: pending_artist.id)
         data = {sampled_song_title: "Put that woman first"}
         patch :update, params: { user_id: user.auth_token, id: @pending_sample.id, data: data }
       end
       it "returns a status of 201" do
         expect(response.status).to eq 201
         expect(@pending_sample.reload.sampled_song_title).to eq "Put that woman first"
       end
     end
  end
end
