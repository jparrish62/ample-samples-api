require 'rails_helper'

RSpec.describe Api::V1::LicensesController, type: :controller do
  describe "#create" do
    before(:each) do
      user = FactoryBot.create(:user, auth_token: SecureRandom.hex)
      client_data      = FactoryBot.attributes_for(:client)
      submission_one   = FactoryBot.attributes_for(:submission)
      submission_two   = FactoryBot.attributes_for(:submission)
      submission_three = FactoryBot.attributes_for(:submission)
      post :create, params: { user_id: user.auth_token, client: client_data, submission: [submission_one, submission_two, submission_three] }
    end
    it "returns a status of 201" do
      expect(response.status).to eq 201
    end
  end
end
