require 'rails_helper'

RSpec.describe Api::V1::ActiveArtistController, type: :controller do
  describe "Update" do
    context "when an active artist is updated" do
      before(:each) do
        user = FactoryBot.create(:user, auth_token: SecureRandom.hex)
        @active_artist = FactoryBot.create(:active_artist)
        FactoryBot.create(:artist_datum, user_id: user.id, active_artist_id: @active_artist.id, pending_artist: nil)
        @data =  { new_artist_name: "Tyler The Creator", new_song_title: "Oh shit", genre: "POP"}
        patch :update, params: { user_id: user.auth_token, data: @data, id: @active_artist.id }, format: :json
      end
      it "creates a pending artist" do
        pending_artist = PendingArtist.find(@active_artist.reload.pending_ref_id)
        expect(pending_artist.reload.new_artist_name).to eq @data[:new_artist_name]
        expect(response.status).to eq 201
      end
    end

    context "when an active artist is updated with an existing copy in a pending state" do
      # -When an active artist is updated and it already has a copy in the pending state
      # It will check current active artist for a reference to copy in pending state
      # If reference exist it will find that reference and update that reference
      before(:each) do
        user = FactoryBot.create(:user, auth_token: SecureRandom.hex)
        @active_artist  = FactoryBot.create(:active_artist)
        @pending_artist = FactoryBot.create(:pending_artist)
        @active_artist.pending_ref_id = @pending_artist.id
        @pending_artist.active_ref_id = @active_artist.id
        @active_artist.save!
        @pending_artist.save!
        FactoryBot.create(:artist_datum, user_id: user.id, active_artist_id: @active_artist.id, pending_artist_id: @pending_artist.id)
        @data = { new_song_published_by: "Arista Records", inspiration: "One hot summer", new_artist_manager: "Puffy" }
        patch :update, params: { user_id: user.auth_token, data: @data, id: @active_artist.id }, format: :json
      end
      it "will find the pending artist and update the pending artist" do
        pending_artist = PendingArtist.find(@pending_artist.id)
        expect(pending_artist.reload.inspiration).to eq @data[:inspiration]
        expect(pending_artist.new_song_published_by).to eq @data[:new_song_published_by]
        expect(response.status).to eq 201
      end
    end
  end
  describe "Create" do
    context "when a new sample is added to an active artist" do
        # -When a sample is added to an active artist
        # It will first check for a reference to a pending copy of the active artist
        # If pending copy exist new sample with attach itself to the pending copy
        # If pending copy does not exist a new pending copy of the active artist will be created
        # Newly created sample will be attached to newly created pending artist
      before(:each) do
        user            = FactoryBot.create(:user, auth_token: SecureRandom.hex)
        @active_artist  = FactoryBot.create(:active_artist)
        @active_sample  = FactoryBot.create(:active_sample, active_artist_id: @active_artist.id)
        FactoryBot.create(:artist_datum, user_id: user.id, active_artist_id: @active_artist.id)
        data = FactoryBot.attributes_for(:pending_sample)
        post :create, params: { user_id: user.auth_token, data: data, id: @active_artist.id }, format: :json
      end
      it "creates artist in pending state and adds the sample" do
        pending_artist = PendingArtist.find(@active_artist.reload.pending_ref_id)
        expect(pending_artist.pending_samples.length).to eq 1
        expect(response.status).to eq 201
      end
    end
    context "when a new sample is added to an active artist and copy already exist in pending state" do
        # -When a sample is added to an active artist
        # It will first check for a reference to a pending copy of the active artist
        # If pending copy exist new sample with attach itself to the pending copy
        # If pending copy does not exist a new pending copy of the active artist will be created
        # Newly created sample will be attached to newly created pending artist
      before(:each) do
        user            = FactoryBot.create(:user, auth_token: SecureRandom.hex)
        @active_artist  = FactoryBot.create(:active_artist)
        @active_sample  = FactoryBot.create(:active_sample, active_artist_id: @active_artist.id)
        @pending_artist = FactoryBot.create(:pending_artist)
        @pending_sample = FactoryBot.create(:pending_sample, pending_artist_id: @pending_artist.id)
        @active_artist.pending_ref_id = @pending_artist.id
        @active_artist.save!
        FactoryBot.create(:artist_datum, user_id: user.id, active_artist_id: @active_artist.id, pending_artist_id: @pending_artist.id)
        data            = FactoryBot.attributes_for(:pending_sample)
        post :create, params: { user_id: user.auth_token, data: data, id: @active_artist.id}
      end
      it "finds artist in pending state and adds the sample" do
        expect(@pending_artist.pending_samples.length).to eq 2
        expect(response.status).to eq 201
      end
    end
  end
  describe "Show" do
    context "when a id is passed in" do
      before(:each) do
        user = FactoryBot.create(:user, auth_token: SecureRandom.hex)
        active_artist = FactoryBot.create(:active_artist)
        active_sample = FactoryBot.create(:active_sample, active_artist_id: active_artist.id)
        FactoryBot.create(:artist_datum, active_artist_id: active_artist.id, user_id: user.id)
        get :show, params: { user_id: user.auth_token, id: active_artist.id }, format: :json
      end
      it "returns artist associated with that id" do
        expect(response.status).to eq 200
      end
    end
  end
  describe "Index" do
    before(:each) do
      user                = FactoryBot.create(:user, auth_token: SecureRandom.hex)
      @active_artist      = FactoryBot.create(:active_artist)
      @active_sample      = FactoryBot.create(:active_sample, active_artist_id: @active_artist.id)
      FactoryBot.create(:artist_datum, user_id: user.id, active_artist_id: @active_artist.id)
      user_two            = FactoryBot.create(:user)
      @active_artist_two  = FactoryBot.create(:active_artist)
      @active_sample_two  = FactoryBot.create(:active_sample, active_artist_id: @active_artist_two.id)
      FactoryBot.create(:artist_datum, user_id: user_two.id, active_artist_id: @active_artist_two.id)
      get :index, format: :json
    end
    it "returns all active artist and associated samples" do
      expect(JSON.parse(response.body).length).to eq 2
      expect(response.status).to eq 200
    end
  end
end
