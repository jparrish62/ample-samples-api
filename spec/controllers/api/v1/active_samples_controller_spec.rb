require 'rails_helper'

RSpec.describe Api::V1::ActiveSamplesController, type: :controller do
# -----------------------------------------------

# If no reference exist
# Copy of the active artist will be created in a pending state
# Updated sample will attach itself to pending  copy
# Active artist will hold a reference to the pending copy
# Pending copy will hold a reference to active copy
# --------------------------------------------------
# -When an active sample is updated and a copy already exist in a pending state
# It will check itself for a reference
# If reference exist
# It will find that pending copy
# Update the attributes of the existing reference

  describe "Update" do
    context "when an active sample is updated" do
      # -When an active sample is updated
      # Find active artist
      # If there’s a reference to a pending copy of the active artist
      # Find pending copy
      # Attach updated copy of active sample to pending copy
      before(:each) do
        user            = FactoryBot.create(:user, auth_token: SecureRandom.hex)
        @active_artist  = FactoryBot.create(:active_artist)
        @active_sample  = FactoryBot.create(:active_sample, sampled_song_title: 'This is the new title', active_artist_id: @active_artist.id)
        artist_data     = FactoryBot.create(:artist_datum, user_id: user.id, active_artist_id: @active_artist.id)
        data = { sampled_song_published_by: "Arista Records", s_inspiration: "One hot summer", sampled_artist_manager: "Puffy" }
        patch :update, params: { user_id: user.auth_token, data: data, id: @active_sample.id }
      end
      it "creates a pending sample" do
        pending_sample = PendingSample.find_by_sampled_song_title('This is the new title')
        expect(pending_sample.reload.sampled_song_title).to eq @active_sample.sampled_song_title
        expect(response.status).to eq 201
      end
    end

    context "when an active sample is updated with an existing copy in a pending state" do
      # -When an active artist is updated and it already has a copy in the pending state
      # It will check current active artist for a reference to copy in pending state
      # If reference exist it will find that reference and update that reference
      before(:each) do
        user            = FactoryBot.create(:user, auth_token: SecureRandom.hex)
        @active_artist  = FactoryBot.create(:active_artist)
        @active_sample  = FactoryBot.create(:active_sample, sampled_song_title: 'Love come down', active_artist_id: @active_artist.id)
        @pending_artist = FactoryBot.create(:pending_artist)
        @pending_sample = FactoryBot.create(:pending_sample, sampled_song_title: "This is why I'm hot", pending_artist_id: @pending_artist.id)
        @active_artist.pending_ref_id = @pending_artist.id
        @active_artist.save
        @pending_artist.active_ref_id = @active_artist.id
        @pending_artist.save
        FactoryBot.create(:artist_datum, user_id: user.id, active_artist_id: @active_artist.id, pending_artist_id: @pending_artist.id)
        @data = { sampled_song_published_by: "Arista Records", s_inspiration: "One hot summer", sampled_artist_manager: "Puffy" }
        patch :update, params: { user_id: user.auth_token, data: @data, id: @active_sample.id}
      end
      it "will find the pending sample and update the pending sample" do
        expect(PendingSample.find_by_s_inspiration("One hot summer").s_inspiration).to eq @data[:s_inspiration]
        expect(response.status).to eq 201
      end
    end
  end
end







