require 'rails_helper'

RSpec.describe Api::V1::AudioUploadsController, type: :controller do 
	describe "#create" do 
			before (:each) do 
				user = FactoryBot.create(:user, auth_token: SecureRandom.hex)
				upload = FactoryBot.build(:audio_upload)
				post :create, params: {user_id: user.auth_token, data: {audio_url: upload.audio_url, image_url: upload.image_url, name: upload.name, trackbio: upload.trackbio, artistbio: upload.artistbio, genre: upload.genre, title: upload.title}}
			end
		it "creates a new audio uploads record" do 
			expect(AudioUpload.all.count).to eq 1
		end
	end

	describe "#show" do 
		before (:each) do 
	  	user   = FactoryBot.create(:user, auth_token: SecureRandom.hex)
	 		upload = FactoryBot.create(:audio_upload, user_id: user.id)
			get :show, params: {id: upload.id, user_id: user.auth_token }, format: :json
		end
		it "returns record record" do 
			expect(response.status).to eq 200
			expect(subject.response_body.count).to eq 1
		end
	end

	describe "#update" do 
     before(:each) do 
	  	 user   = FactoryBot.create(:user, auth_token: SecureRandom.hex)
			 @upload = FactoryBot.create(:audio_upload, user_id: user.id)
			 data = {name: "Joseph Brown"} 
			 put :update, params: {id: @upload.id, data: data}, format: :json
		 end
		 it "updates existing record" do 
			 expect(@upload.reload.name).to eq "Joseph Brown"
		 end
	end

	describe "#destoy" do 
		before(:each) do 
	    user   = FactoryBot.create(:user, auth_token: SecureRandom.hex)
			@upload = FactoryBot.create(:audio_upload, user_id: user.id)
			delete :destroy, params: {id: @upload.id}, format: :json
		end
		it "destroys specified record" do 
		  expect(AudioUpload.all).to eq [] 
		end
	end
end
