require 'rails_helper'

RSpec.describe Api::V1::TokensController, type: :controller do

  describe "generate token" do
    before(:each) do
      token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5VRkJPRFJDTUVWRk4wVkNOa1ZDT0RrM1F6UTRNVGc1TmtKQ01qVXdRakl6TURrNU5EZEZOQSJ9.eyJuaWNrbmFtZSI6InBhcnJpc2hqdWxpYW42MiIsIm5hbWUiOiJwYXJyaXNoanVsaWFuNjJAZ21haWwuY29tIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyLzg3ZTJiMDgwMTJmMmVhN2YzYTEyNjliNDM0NzhkNzQ2P3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGcGEucG5nIiwidXBkYXRlZF9hdCI6IjIwMTktMDMtMTRUMTI6NTI6MTguMTYzWiIsImlzcyI6Imh0dHBzOi8vcmVhY3RhdXRoZW50aWMuZXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVjODk2MjRmMDNlOWYzNDBlYTVlOWQ4MyIsImF1ZCI6InFycUQydUQ0b3lGUHpkNW5ERmd3UUpGWDRtNHUydWZaIiwiaWF0IjoxNTUyNTY3OTM4LCJleHAiOjE1NTI2MDM5MzgsImF0X2hhc2giOiJKVng0bFYxdE5QRERWdTlGeTlrNjB3Iiwibm9uY2UiOiJDd1oyUHlQTS5QTEEwZEEzS1Rndk9TSzRvRm4ycXJ2ZCJ9.KLjByXe_bkrEDGO-uofoukaHSIZ6jGXsz6HOF1wcVXjbSdPGwVumLI0BvqaX47zhpQKPsBRD5zmu7SMG7bNFoTPeaIwMoCwxnhjmOkdWHzS0BgnwtxE1Qt4qOecsQzH47MXIi1k-0F0zclzo6KasTpi1TDeRSHjMrFWoE46YvrUIPnhYCRGvblBKCTavVRVR6B-JF_-rE1WAMdidOvoZJnPyg5PGQGuJQhaednJhTDouX510Gj6Q_TDTIrMDiTiUfi8_ViCERPqDWgTxMkOM-27ngNZsSKvleX-H2DvX98C3wUkr2lCzF7DT3TeDN_zjng-Rc5O30bSNo3fIM2YqmQ"
      post :create, params: {Authorize: token}, format: :json
    end
    context "when a user does not exist" do
      it "returns a token from rails" do
        expect(json_response[:jwt].present?).to eq true
        expect(response.status).to eq 201
        expect(User.last.auth_token.present?).to eq true
      end
    end
  end

  describe "generate token" do
    before(:each) do
      token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZOMFk0UTBNeE1qSXdPVVF6UVVOR016WkZOa0V6UVRnd1JUYzFRelpFTVVJMU9FVXlSZyJ9.eyJnaXZlbl9uYW1lIjoiSm9zaHVhIiwiZmFtaWx5X25hbWUiOiJQbGljcXVlIiwibmlja25hbWUiOiJwbGljam8iLCJuYW1lIjoiSm9zaHVhIFBsaWNxdWUiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDQuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy15TWdubURsVHgxOC9BQUFBQUFBQUFBSS9BQUFBQUFBQUF0dy96N3IzWGRyWHpMOC9waG90by5qcGciLCJnZW5kZXIiOiJtYWxlIiwibG9jYWxlIjoiZW4iLCJ1cGRhdGVkX2F0IjoiMjAxOS0wMy0yN1QxNDowMjowMy4yNDlaIiwiaXNzIjoiaHR0cHM6Ly9hbXBsZXNhbXBsZXMuYXV0aDAuY29tLyIsInN1YiI6Imdvb2dsZS1vYXV0aDJ8MTEwOTM1NTU3ODQ2MzI2NDUyMjY0IiwiYXVkIjoiM0M2QTBOSFZEaGMyS0k3M2x0N0M5SGN6UUlLVzZFeTIiLCJpYXQiOjE1NTM2OTUzMjMsImV4cCI6MTU1MzczMTMyMywiYXRfaGFzaCI6IjV1QUtkYkVWSWhjV2JBcGpsRjd1OUEiLCJub25jZSI6Ik5jMHkwSjNtNGdnbnpxa0hnODhRfjd-flFyTHNLYVVPIn0.1ygVvcXHGavQCYlw8a3G_DaeAP9G8lUpkhpUXE5gTHsEoAdZYCF5bye--RtHtZhuBMmrQFJYuf4TnGwzhSuRk5oRiLS3oDt79oTy72Ff83QeQRKUh4iLcNtxUVAIBK5-jSrMhFrmrzOhgQI1yb1N4txWtN7inkQv4YQPv9rxZCoqNwLKfDtn4APBagxullfeyvrT61lsI99J68chE-aBzwH0Lrscy5gbQFhCQJe_OmKFqD0aGB2vQGOOi8yD16nIeWPmVygVDKpQKSPOQbNV52EcfG3BNEg3kGaK5-bxGvSLJIqtjam6eKbShmTitThHY6vS1Pb6npbvBSfN4sbbkw"
      post :create, params: {Authorize: token}, format: :json
    end
    context "when a user does not exist" do
      it "returns a token from rails" do
        expect(json_response[:jwt].present?).to eq true
        expect(User.last.auth_token.present?).to eq true
      end
    end
  end

  describe "generate token" do
    before(:each) do
      user = User.new(name: 'parrishjulian62@gmail.com', nickname: "parrishjulian62", password_digest: SecureRandom.hex)
      user.save!
      token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5VRkJPRFJDTUVWRk4wVkNOa1ZDT0RrM1F6UTRNVGc1TmtKQ01qVXdRakl6TURrNU5EZEZOQSJ9.eyJuaWNrbmFtZSI6InBhcnJpc2hqdWxpYW42MiIsIm5hbWUiOiJwYXJyaXNoanVsaWFuNjJAZ21haWwuY29tIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyLzg3ZTJiMDgwMTJmMmVhN2YzYTEyNjliNDM0NzhkNzQ2P3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGcGEucG5nIiwidXBkYXRlZF9hdCI6IjIwMTktMDMtMTRUMTI6NTI6MTguMTYzWiIsImlzcyI6Imh0dHBzOi8vcmVhY3RhdXRoZW50aWMuZXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVjODk2MjRmMDNlOWYzNDBlYTVlOWQ4MyIsImF1ZCI6InFycUQydUQ0b3lGUHpkNW5ERmd3UUpGWDRtNHUydWZaIiwiaWF0IjoxNTUyNTY3OTM4LCJleHAiOjE1NTI2MDM5MzgsImF0X2hhc2giOiJKVng0bFYxdE5QRERWdTlGeTlrNjB3Iiwibm9uY2UiOiJDd1oyUHlQTS5QTEEwZEEzS1Rndk9TSzRvRm4ycXJ2ZCJ9.KLjByXe_bkrEDGO-uofoukaHSIZ6jGXsz6HOF1wcVXjbSdPGwVumLI0BvqaX47zhpQKPsBRD5zmu7SMG7bNFoTPeaIwMoCwxnhjmOkdWHzS0BgnwtxE1Qt4qOecsQzH47MXIi1k-0F0zclzo6KasTpi1TDeRSHjMrFWoE46YvrUIPnhYCRGvblBKCTavVRVR6B-JF_-rE1WAMdidOvoZJnPyg5PGQGuJQhaednJhTDouX510Gj6Q_TDTIrMDiTiUfi8_ViCERPqDWgTxMkOM-27ngNZsSKvleX-H2DvX98C3wUkr2lCzF7DT3TeDN_zjng-Rc5O30bSNo3fIM2YqmQ"
      post :create, params: {Authorize: token}, format: :json
    end
    context "when user exist" do
      it "returns a token from rails" do
        expect(User.last.name).to eq  'parrishjulian62@gmail.com'
        expect(User.last.auth_token.present?).to eq true
        expect(json_response[:jwt].present?).to eq true
      end
    end
  end

  describe "log out" do
    before(:each) do
      @user = User.new(name: 'parrishjulian62@gmail.com', nickname: "parrishjulian62", password_digest: SecureRandom.hex, auth_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5VRkJPRFJDTUVWRk4wVkNOa1ZDT0RrM1F6UTRNVGc1TmtKQ01qVXdRakl6TURrNU5EZEZOQSJ9")
      @user.save!
      token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5VRkJPRFJDTUVWRk4wVkNOa1ZDT0RrM1F6UTRNVGc1TmtKQ01qVXdRakl6TURrNU5EZEZOQSJ9"
      delete :destroy, params: {id: token, token: {id: token}}, format: :json
    end
    context "when a user logs out" do
      it "destroys auth token" do
        expect(User.find(@user.id).auth_token).to eq ""
      end
    end
  end
end

