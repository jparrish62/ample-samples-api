require 'rails_helper'

RSpec.describe Api::V1::PendingArtistController, type: :controller do
  describe "Create" do
  # Pending artist is created
  # It creates artist in a pending state
  # It creates sample in a pending state
  # ------------------------------------------------
    context "when a pending artist is created" do
      before(:each) do
        user = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: 'admin')
        pending_artist = FactoryBot.attributes_for(:pending_artist)
        pending_sample = FactoryBot.attributes_for(:pending_sample)
        post :create, params: { user_id: user.auth_token, pending_artist: pending_artist, pending_sample: pending_sample, user_id: user.auth_token}
      end
        it "returns status of 201" do
          expect(response.status).to eq 201
        end
     end
  end
#   When an artist and all samples are approved
# It creates artist and samples in the active state
# It destroys itself
# -------------------------------------------------
 describe "approve_artist"  do
   context "when an artist and all samples are approved" do
       before(:each) do
         user               = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: "admin")
         @pending_artist    = FactoryBot.create(:pending_artist, new_artist_name: 'nas')
         FactoryBot.create(:artist_datum, user_id: user.id, pending_artist_id: @pending_artist.id)
         pending_sample     = FactoryBot.create(:pending_sample, sampled_artist_name: 'james brown', pending_artist_id: @pending_artist.id)
         pending_sample_two = FactoryBot.create(:pending_sample, sampled_artist_name: 'little richie', pending_artist_id: @pending_artist.id)
         post :approve_all, params: {user_id: user.auth_token, id: @pending_artist.id}
       end
       it "returns a status of 201" do
         pending_artist = PendingArtist.find(@pending_artist.id)
         active_artist  = ActiveArtist.find_by_new_artist_name(@pending_artist.new_artist_name)
         expect(response.status).to eq 201
         expect(active_artist.new_artist_name).to eq @pending_artist.new_artist_name
       end
     end
  end
#   When artist is approved but not all samples are approved
# It creates a copy in an active state
# It creates approved samples
# Pending samples remain in a pending state
# Copy of pending artist remains in pending state
 # ----------------------------------------------
#   When artist exist in an active state and pending state samples are approved but not all samples are approved
# It finds artist in active state
# It adds approved samples to active artist
# Copy of sample remains in pending state
# Unapproved samples remain in pending state
# --------------------------------------------------------

   describe "approve_selections_pending" do
     context "when artist is approved but not all samples" do
       before(:each) do
         user               = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: "admin")
         @pending_artist    = FactoryBot.create(:pending_artist)
         FactoryBot.create(:artist_datum, user_id: user.id, pending_artist_id: @pending_artist.id)
         pending_sample     = FactoryBot.create(:pending_sample, pending_artist_id: @pending_artist.id)
         pending_sample_two = FactoryBot.create(:pending_sample, pending_artist_id: @pending_artist.id)
         post :approve, params: { user_id: user.auth_token, artist_id: @pending_artist.id, col: pending_sample.id.to_s}
       end
       it "returns a status of 201" do
         @pending_artist = PendingArtist.find(@pending_artist.id)
         active_artist  = ActiveArtist.find_by_new_song_title(@pending_artist.new_song_title)
         expect(response.status).to eq 201
         expect(active_artist.new_artist_name).to eq @pending_artist.new_artist_name
       end
     end

     context "when artist is approved but not all samples approved, copy in active state exist" do
       before(:each) do
         user               = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: "admin")
         @active_artist     = FactoryBot.create(:active_artist)
         @pending_artist    = FactoryBot.create(:pending_artist)
         FactoryBot.create(:artist_datum, user_id: user.id, pending_artist_id: @pending_artist.id)
         pending_sample     = FactoryBot.create(:pending_sample, pending_artist_id: @pending_artist.id)
         pending_sample_two = FactoryBot.create(:pending_sample, pending_artist_id: @pending_artist.id)
         active_sample      = FactoryBot.create(:active_sample,  active_artist_id:  @active_artist.id)
         @active_artist.pending_ref_id = @pending_artist.id
         @active_artist.save
         @pending_artist.active_ref_id = @active_artist.id
         @pending_artist.save
         post :approve, params: { user_id: user.auth_token, artist_id: @pending_artist.id, col: pending_sample.id.to_s }
       end
       it "returns a status of 201" do
         active_artist  = ActiveArtist.find_by_new_artist_name(@pending_artist.new_artist_name)
         expect(response.status).to eq 201
         expect(active_artist.new_artist_name).to eq @pending_artist.new_artist_name
       end
     end
   end
   describe "Update" do
      # -------------------------------------------------
      #   When an artist is updated
      # It finds that artist
      # It updates attributes associated with that artist
      # --------------------------------------------------
     context "when an artist is updated" do
       before (:each) do
         user            = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: "admin")
         @pending_artist = FactoryBot.create(:pending_artist)
         pending_sample  = FactoryBot.create(:pending_sample, pending_artist_id: @pending_artist.id)
         FactoryBot.create(:artist_datum, user_id: user.id, pending_artist_id: @pending_artist.id)
         data = {new_song_written_by: "Jaheim"}
         patch :update, params: { user_id: user.auth_token, id: @pending_artist.id, data: data }
       end
       it "returns a status of 201" do
         expect(response.status).to eq 201
         expect(@pending_artist.reload.new_song_written_by).to eq "Jaheim"
       end
     end
   end
   describe "Show" do
    #   When an artist is shown
    # It finds that artist
    # It returns that artist
    # It returns samples associated with that artist
    # ---------------------------------------------------------
     context "when an artist is shown" do
       before (:each) do
         user = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: "admin")
         @pending_artist = FactoryBot.create(:pending_artist)
         pending_sample = FactoryBot.create(:pending_sample, pending_artist_id: @pending_artist.id)
         FactoryBot.create(:artist_datum, user_id: user.id, pending_artist_id: @pending_artist.id)
         get :show, params: { user_id: user.auth_token, id: @pending_artist.id }, format: :json
       end
       it "returns artist" do
         expect(response.status).to eq 200
       end
     end
   end

   describe "Destroy" do
      #   When an artist is destroyed
      # It finds that artist
      # It destroys that artist
      # It destroys its dependencies
      # --------------------------------------------------------
     context "when an artist is destroyed" do
         before(:each)  do
           user = FactoryBot.create(:user, auth_token: SecureRandom.hex, role: "admin")
           @pending_artist = FactoryBot.create(:pending_artist)
           pending_sample  = FactoryBot.create(:pending_sample, pending_artist_id: @pending_artist.id)
           FactoryBot.create(:artist_datum, user_id: user.id, pending_artist_id: @pending_artist.id)
           delete :destroy, params: { user_id: user.auth_token, id: @pending_artist.id }, format: :json
         end
        it "removes artist from db" do
          expect(response.status).to eq 204
        end
      end
   end
end
