FactoryBot.define do 
	factory :audio_upload do 
   image_url {"string"} 
	 audio_url {"string"}
	 name {"name"}
	 title {"title"}
	 trackbio {"text"}
	 genre {"string"}
	 artistbio {"string"}
	 user_id { nil }
	end
end
