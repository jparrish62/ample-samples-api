FactoryBot.define do
  factory :submission do
     artist_name {"String"}
     song_name {"String"}
     song_link {"String"}
     writers  {"String"}
     publishers  {"String"}
     splits  {"String"}
     sampled_song   {"String"}
     link_sampled_song   {"String"}
     sampled_song_label  {"String"}
     writers_sampled_song  {"String"}
     sampled_publishing   {"String"}
     sample_description   {"String"}
     master           {"String"}
     publishing_sample_used {"String"}
  end
end
