FactoryBot.define do
  factory :active_artist do
    new_artist_name { "MyString" }
    new_song_title { "MyString" }
    new_song_executive_producer { "MyString" }
    new_song_youtube_url { "MyString" }
    new_song_published_by { "MyString" }
    new_artist_manager { "MyString" }
    new_artist_label { "MyString" }
    new_song_produced_by { "MyString" }
    new_song_written_by { "MyString" }
    inspiration { "MyText" }
    image_url { "MyString" }
    genre { "MyString" }
  end
end
