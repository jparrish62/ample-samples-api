FactoryBot.define do
  factory :user do
    name { "MyString" }
    nickname { "MyString" }
    auth_token { "MyText" }
    password_digest { "MyText" }

    trait :admin do
      role {'admin'}
    end
  end
end
