FactoryBot.define do
  factory :client do
      label { "String" }
      distributer { "String" }
      contact_name { "String"}
      company { "String"}
      signer {"String"}
      payee { "String" }
      email { 'String' }
      phone_number { "String" }
      clearance_deadline { "String" }
  end
end
