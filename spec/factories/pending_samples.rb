FactoryBot.define do
  factory :pending_sample do
    sampled_artist_name { "MyString" }
    sampled_song_title { "MyString" }
    sampled_song_executive_producer { "MyString" }
    sampled_song_published_by { "MyString" }
    sampled_artist_manager { "MyString" }
    sampled_song_youtube_url { "MyString" }
    sampled_artist_label { "MyString" }
    sampled_song_produced_by { "MyString" }
    s_inspiration { "MyText" }
    s_genre { "MyString" }
    sampled_song_written_by { "MyString" }
  end
end
