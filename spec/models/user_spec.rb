require 'rails_helper'

RSpec.describe User, type: :model do
  context "when a user has a valid token" do
    user = FactoryBot.create(:user, auth_token: SecureRandom.hex)
    it "returns user" do
      expect(described_class.find_user(user.auth_token).id).to eq user.id
    end
  end

  context "when a user does not have a secure password" do
    user = FactoryBot.create(:user, auth_token: nil)
    it "return a flash message" do
      expect(described_class.find_user(user.auth_token)).to eq nil
    end
  end
end
