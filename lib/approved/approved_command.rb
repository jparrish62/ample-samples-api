module Approved
  class ApprovedCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @user_id    = params[:user_id]
      @artist_id  = params[:artist_id]
      @collection = params[:col]
    end

    def call
      pending = PendingArtist.find_pending_artist(artist_id)
      if pending.active_ref_id
        add_samples(pending)
				remove_from_artist_datum(pending)
        destroy_pending(pending)
      else
        create_samples(pending)
				remove_from_artist_datum(pending)
        destroy_pending(pending)
      end
    end

    def create_samples(pending)
      artist = create_active_artist(pending)
      set_artist_datum(pending, artist)
      collection.split(', ').each do |id|
        sample = PendingSample.find_pending_sample(id)
        ActiveSample.create_active_samples(artist, sample)
      end
    end

    def add_samples(artist)
      active = ActiveArtist.active_artist(artist.active_ref_id)
      collection.split(', ').each do |id|
        sample = PendingSample.find_pending_sample(id)
        ActiveSample.create_active_samples(active, sample)
      end
    end

    def create_active_artist(artist)
      ActiveArtist.create_active_artist(artist)
    end

    private
    attr_reader :user_id, :artist_id, :collection

    def set_artist_datum(pending, artist)
      datum = ArtistDatum.find_artist_datum_pending_id(pending.id)
      datum.active_artist_id = artist.id
      datum.save!
    end

		def remove_from_artist_datum(pending)
			datum = ArtistDatum.find_artist_datum_pending_id(pending.id)
      datum.pending_artist_id = nil 
			datum.save
		end

    def destroy_pending(pending)
      PendingArtist.destroy_pending(pending)
    end
  end
end
