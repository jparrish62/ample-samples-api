module Approved
  class ApprovedAllCommand

    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @artist_id = params[:id]
    end

    def call
      pending = PendingArtist.find_pending_artist(artist_id)
      if pending.active_ref_id
        update_active(pending)
        add_samples(pending)
				remove_from_artist_datum(pending)
        destroy_pending(pending)
      else
        create_samples(pending)
				remove_from_artist_datum(pending)
        destroy_pending(pending)
      end
    end

    def update_active(artist)
      active = ActiveArtist.active_artist(artist.active_ref_id)
			active.update_attributes(t_hash(artist).except('active_ref_id', 'id'))
    end

    def create_samples(artist)
      active = create_active_artist(artist.attributes)
      set_artist_datum(artist, active)
      collection = artist.pending_samples.pluck(:id)
      collection.each do |id|
        sample = PendingSample.find_pending_sample(id)
        ActiveSample.create_active_samples(active, sample)
      end
    end

    def add_samples(artist)
      active = ActiveArtist.active_artist(artist.active_ref_id)
      collection = artist.pending_samples.pluck(:id)
      collection.each do |id|
        sample = PendingSample.find_pending_sample(id)
        if sample.active_sample_ref_id
          ActiveSample.update_active_sample(sample)
        else
         ActiveSample.create_active_samples(active, sample)
        end
      end
    end

    def create_active_artist(artist)
      ActiveArtist.create_active_artist(artist)
    end

    private

    attr_reader :artist_id

    def set_artist_datum(artist, active)
      datum = ArtistDatum.find_artist_datum_pending_id(artist.id)
      datum.active_artist_id = active.id
      datum.save!
    end

    def destroy_pending(pending)
      PendingArtist.destroy_pending(pending)
    end

		def remove_from_artist_datum(pending)
			datum = ArtistDatum.find_artist_datum_pending_id(pending.id)
      datum.pending_artist_id = nil 
			datum.save
		end
  end

	private 

	def self.t_hash(data)
		if data.class == Hash
			data 
		else
			data.attributes.to_h
		end
	end
end
