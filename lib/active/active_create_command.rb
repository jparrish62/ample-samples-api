module Active
 class ActiveCreateCommand
   def self.call(params)
     new(params).call
   end

   def initialize(params)
    @id =    params[:id]
    @data =  params[:data]
   end

   def call
      active = ActiveArtist.active_artist(id)
      if active.pending_ref_id
        pending = PendingArtist.find_pending_artist(active.pending_ref_id)
        PendingArtist.add_sample(pending, data)
      else
        pending = PendingArtist.create_pending_artist_from_active(active)
        ActiveArtist.set_pending_ref_id(active, pending)
        ArtistDatum.set_pending_id_to_artist_datum(active, pending)
        PendingArtist.add_sample(pending, data)
      end
   end

   private
   attr_reader :id, :data
 end
end
