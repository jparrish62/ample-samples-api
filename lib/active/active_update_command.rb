module Active
  class ActiveUpdateCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @id   = params[:id]
      @data = params[:data]
    end

    def call
      artist = ActiveArtist.active_artist(id)
      if artist.pending_ref_id
        PendingArtist.update_pending(artist.pending_ref_id, data)
      else
        pending = PendingArtist.create_pending_artist_from_active(artist)
        ActiveArtist.set_pending_ref_id(artist, pending)
        ArtistDatum.set_pending_id_to_artist_datum(artist, pending)
        PendingArtist.update_pending(pending.id, data)
      end
    end

    private

    attr_reader :id, :data
  end
end
