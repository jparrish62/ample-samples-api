module Active
   class ActiveShowCommand

     def self.call(params)
        new(params).call
     end

     def initialize(args)
        @id = args[:id]
     end

     def call
       ActiveArtist.active_artist(id)
     end

     private
     attr_reader :id
   end
end
