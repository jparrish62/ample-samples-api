module Upload
	class AudioUploadsDestroyCommand

		def self.call(params)
			new(params).call
		end

		def initialize(params)
			@id = params[:id]
		end

		def call
			find_record.destroy!
		end

		def find_record
			AudioUpload.find_record(id)
		end

		private 

		attr_accessor :id
	end
end
