module Upload
 class AudioUploadsCreateCommand
	 def self.call(params)
		 new(params).call
	 end

	 def initialize(params)
		 @token     = params[:user_id]
		 @data      = params[:data]
	 end

	 def call
     create_upload_record 
	 end

	 def create_upload_record
     AudioUpload.create_record(current_user, data) 
	 end

	 def current_user 
		 User.find_user(token)
	 end

	 private 

	 attr_reader :token, :data
 end
end
