module Upload
	class AudioUploadsShowCommand
		def self.call(params)
			new(params).call
		end

		def initialize(params)
			@token = params[:user_id]
			@id    = params[:id]
			@array = []
		end

		def call
		  #array << index	
		  array << upload	
			array
		end

		def upload
			AudioUpload.find_record(id)
		end

		def index
      current_user.audio_uploads
		end

		def current_user
			User.find_user(token)
		end

		private 
		attr_reader :token, :id, :array
	end
end
