module Upload
	class AudioUploadsUpdateCommand
    def self.call(params)
			new(params).call
	  end

		def initialize(params) 
      @data = params[:data]
			@id   = params[:id]
		end

		def call
			update_record
		end

		def find_record
			AudioUpload.find_record(id)
		end

		def update_record
      find_record.update_attributes(data.permit!)
		end

	  private 	
		attr_reader :id, :data
	end
end
