module Token
  class DestroyTokenCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @token = params[:token][:id]
    end

    def call
      user = User.find_by_auth_token(token)
      user['auth_token'] = ""
      user.save!
    end

    private
    attr_reader :token
  end
end

