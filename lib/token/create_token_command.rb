module Token
  class CreateTokenCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @token = params[:Authorize]
    end

    def call
      credentials = decode_token(token)
      valid_credentials(credentials)
    end

    def valid_credentials(credentials)
      user = User.find_by_nickname(credentials.first['nickname'])
      if user
        new_token = encode_token({id: user.id, nickname: user.nickname, name: user.name})
        user['auth_token'] = new_token
        user.save
        user
      else
        create_user(credentials)
      end
    end

    def create_user(credentials)
      user = User.new(name: credentials.first['name'], nickname: credentials.first['nickname'], password_digest: SecureRandom.hex)
      if user.save && valid_domain(credentials.first['iss'])
        new_token = encode_token({id: user.id, nickname: user.nickname, name: user.name})
        user['auth_token'] = new_token
        user.save
        user
      end
    end

    private
    attr_reader :token

    def encode_token(payload={})
      exp = 24.hours.from_now
      payload[:exp] = exp.to_i
      JWT.encode(payload, Rails.application.secret_key_base)
    end

    def valid_domain(domain)
      domain == Rails.application.secrets.auth0_domain || Rails.application.secrets.auth0_domain2
    end

    def decode_token(token)
      JWT.decode token, nil, false
    end
  end
end
