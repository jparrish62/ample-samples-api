module ActiveSamples
  class  ActiveSampleUpdateCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @sample_id = params[:id]
      @data      = params[:data]
    end

    def call
      sample  = ActiveSample.find_active_sample(sample_id)
      artist  = ActiveArtist.active_artist(sample.active_artist.id)
      if artist.pending_ref_id
        pending = PendingArtist.find_pending_artist(artist.pending_ref_id)
        PendingSample.update_sample(pending, sample, data)
      else
        pending = PendingArtist.create_pending(artist)
        PendingSample.update_sample(pending, sample, data)
        ActiveArtist.set_pending_ref_id(artist, pending)
      end
    end

    private
    attr_reader :sample_id, :data
  end
end
