module PendingSamples
  class PendingSamplesUpdateCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @sample_id = params[:id]
      @data      = params[:data]
    end

    def call
      sample = PendingSample.find_pending_sample(sample_id)
      sample.update(data.permit!)
    end

    private
    attr_reader :sample_id, :data
  end
end
