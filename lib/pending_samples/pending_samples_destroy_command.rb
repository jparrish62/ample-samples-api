module PendingSamples
  class PendingSamplesDestroyCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @collection = params[:id]
    end

    def call
      collection.each do |id|
        PendingSample.destroy_sample(id)
      end
    end

    private
    attr_reader :collection
  end
end
