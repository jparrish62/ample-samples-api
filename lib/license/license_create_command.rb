module License
  class LicenseCreateCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @token       = params[:user_id]
      @client      = params[:client]
      @submission  = params[:submission]
    end

    def call
      gen_client
      persist_submission
    end

    private
    attr_reader :token, :client, :submission

    def gen_client
      Client.create_client(token, client)
    end

    def persist_submission
      Submission.create_submission(gen_client, submission)
    end
  end
end
