module Stripe
	class StripePaymentsCreateCommand
		require 'stripe'

		def self.call(params)
			new(params).call
		end

		def initialize(params)
      @token     = params[:token]
			@addresses = params[:addresses]
			@amount    = params[:amount]
			@artist_id = params[:artist_id]
		end

		def call
      Stripe.api_key = 'sk_test_ersupjq34dHaBrOwhh8FkY0H00A5TFqq76'
			charge
		end

		def customer
      Stripe::Customer.create(:email => token[:email], :source => token[:id])
		end

    def charge
			begin	 
       charge = Stripe::Charge.create(
        :customer => customer.id,
        :amount => amount,
        :description =>  'Rails Stripe customer',
        :currency => 'usd'
       )

     rescue Stripe::CardError => e
			 'Error with your charge'
		 end
   end

		def send_down_load
			SendDownLoadLink::DownLoadLink.generate_link(artist.audio_url)
		end

		private 
		attr_reader :token, :addresses, :amount, :artist_id
    
		def artist
	    AudioUpload.find_record(artist_id) 	
		end

	end
end





