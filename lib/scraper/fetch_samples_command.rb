require 'nokogiri'
require 'mechanize'
require 'open-uri'
module Scraper
  class FetchSamplesCommand
   attr_reader :agent, :page, :artist, :samples, :urls, :song_title

    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @artist           =  params[:artist]
      @song_title       =  params[:song_title]
      @title            =  params[:title]
      @agent            =  Mechanize.new
      @samples          =  Array.new
      @urls             =  Array.new
      @page           ||=  @agent.get("https://www.whosampled.com")
    end

    def call
      title_search
      results
    end

    def title_search
      form          = page.forms.first
      form['query'] = song_title
      form['q']     = song_title
      @results      = agent.submit(form)
    end

    def results
      @results.links.each do |link|
        if /#{artist}/.match(link.text) && /sample/.match(link.text)
          samples << link.text
          urls <<  "https://www.whosampled.com" + link.href
        end
      end
      persist_results
      samples
    end

    private
    def persist_results
			Sample.create(artist:artist, song_title:song_title, song_samples:samples.join(','), youtube_links: urls.join(',')) if !Sample.find_by_song_title(song_title).present? && !samples.empty?
    end
  end
end
