require 'nokogiri'
require 'mechanize'
require 'open-uri'

module Scraper
  class FetchLinksCommand

    def self.call(params)
      new(params).call
    end

    def initialize(params)
     @song_title       =  params[:song_title]
     @title            =  params[:title]
     @video_links      =  video_links
     @new_collection   =  Array.new
     @final_collection =  Array.new
    end

    def call
      collection
      link_search
    end

    def collection
      Sample.find_by_song_title(song_title).youtube_links.split('  ').reject { |s| s.empty? }.each do |string|
        new_collection << string.gsub("\n", '')
      end
      new_collection
    end

    def link_search
      new_collection.each do |url|
        document = open(url)
        content  = document.read
        parsed_content = Nokogiri::HTML(content)
        final_collection << "https://www.youtube.com/embed/" + parsed_content.css("body").css("main").css("#content").css(".divided-layout").first.css('iframe').first.values.first
        final_collection << "https://www.youtube.com/embed/" + parsed_content.css("body").css("main").css("#content").css(".divided-layout").first.css('iframe').last.values.first
      end
      final_collection
    end

    private
    attr_accessor  :new_collection, :final_collection, :song_title, :video_links, :title
  end
end
