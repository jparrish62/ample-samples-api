module CurrentUser
  class CurrentAdminCommand

    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @user_id = params[:user_id]
    end

    def call
      if current_user&.admin?
        current_user
      else
        raise "This area is restricted for admins"
      end
    end

    private
    attr_reader :user_id

    def current_user
      User.find_user(user_id)
    end
  end
end
