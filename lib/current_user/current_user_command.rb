module CurrentUser
  class CurrentUserCommand 

    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @user_id = params[:user_id]
    end

    def call
			if current_user
				current_user
			else
        raise "Please SignUp Or SignIn" 
			end
    end

    private
    attr_reader :user_id, :message

    def current_user
      return User.find_user(user_id)
    end
  end
end
