module Pending
  class PendingUpdateCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @artist_id = params[:id]
      @data      = params[:data]
    end

    def call
      PendingArtist.update_pending(artist_id, data)
    end

    private
    attr_reader :artist_id, :data
  end
end
