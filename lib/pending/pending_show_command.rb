module Pending
  class PendingShowCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @id = params[:id]
    end

    def call
      PendingArtist.find_pending_artist(id)
    end

    private
    attr_reader :id
  end
end
