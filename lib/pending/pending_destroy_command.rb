module Pending
  class PendingDestroyCommand
    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @artist_id = params[:id]
    end

    def call
      if pending.active_ref_id
        ActiveArtist.remove_pending_ref_id(pending.active_ref_id)
        ArtistDatum.remove_pending_id(pending.active_ref_id)
		    remove_from_artist_datum(pending)
        PendingArtist.destroy_pending(pending)
      else
        ArtistDatum.remove_pending_id(pending.id)
		    remove_from_artist_datum(pending)
        PendingArtist.destroy_pending(pending)
      end
    end

    private
    attr_reader :artist_id

		def remove_from_artist_datum(pending)
			datum = ArtistDatum.find_artist_datum_pending_id(pending.id)
      datum.pending_artist_id = nil 
			datum.save
		end

    def pending
      PendingArtist.find_pending_artist(artist_id)
    end
  end
end
