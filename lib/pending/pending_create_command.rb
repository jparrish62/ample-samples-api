module Pending
  class PendingCreateCommand

    def self.call(params)
      new(params).call
    end

    def initialize(params)
      @token          = params[:user_id]
      @pending_artist = params[:pending_artist]
      @pending_sample = params[:pending_sample]
    end

    def call
      pending = create_pending
      ArtistDatum.create_artist_datum(user.id, pending.id)
      PendingArtist.add_sample_to_pending(pending.id, pending_sample)
    end

    private
    attr_reader :token, :pending_artist, :pending_sample

    def user
       User.find_user(token)
    end

    def create_pending
      PendingArtist.create_pending(pending_artist)
    end
  end
end
