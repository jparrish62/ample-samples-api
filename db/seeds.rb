# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#


user = User.create!(
 name: "John Finn",
 nickname: "john@example.com",
 password_digest: SecureRandom.hex,
 role: "admin"
)
user_two = User.create!(
 name: "BIlly Sims",
 nickname: "billy@example.com",
 password_digest: SecureRandom.hex,
 role: "user"
)
user_three = User.create!(
 name: "Travis Scott",
 nickname: "travis@example.com",
 password_digest: SecureRandom.hex,
 role: "user"
)
user_four = User.create!(
 name: "Ben Scott",
 nickname: "ben@example.com",
 password_digest: SecureRandom.hex,
 role: "user"
)
user_five = User.create!(
 name: "Greg",
 nickname: "greg@example.com",
 password_digest: SecureRandom.hex,
 role: "admin"
)

  artist = ActiveArtist.create!(
     new_artist_name: "Tupac Sukur",
     new_song_title: "Dear Momma",
     new_song_executive_producer: "Arista Records",
     new_song_youtube_url: "https://www.youtube.com/watch?v=Mb1ZvUDvLDY",
     new_song_published_by: "Jive",
     new_artist_manager: "Benny Boom",
     new_artist_label: "Death Row",
     new_song_produced_by: "Dr. Dre",
     new_song_written_by: "Tupac",
     inspiration: "Hot summer in Harlem",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1562975843/artistImages/wcvsaoys6tcu2kvltsed.jpg",
     genre: "Hip Hop"
  )
  artist_two = ActiveArtist.create!(
     new_artist_name: "Big Daddy Kane",
     new_song_title: "Long Live The Kane",
     new_song_executive_producer: "Marley Marl",
     new_song_youtube_url: "https://www.youtube.com/watch?v=Mb1ZvUDvLDY",
     new_song_published_by: "Jive",
     new_artist_manager: "Klark Kent",
     new_artist_label: "Cold Chillin",
     new_song_produced_by: "Marley Marl",
     new_song_written_by: "Big Daddy Kane",
     inspiration: "I first heard this join as a young kid growing up in Brooklyn",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1555981440/artistImages/ljd3fbymjbqwcbj3ita6.jpg",
     genre: "Hip Hop"
  )
  artist_three = ActiveArtist.create!(
     new_artist_name: "Beyonce",
     new_song_title: "Love on top",
     new_song_executive_producer: "Pharell",
     new_song_youtube_url: "https://www.youtube.com/watch?v=Ob7vObnFUJc",
     new_song_published_by: "Sony",
     new_artist_manager: "Benny Boom",
     new_artist_label: "Sony",
     new_song_produced_by: "Beyonce",
     new_song_written_by: "Beyonce",
     inspiration: "First heard this joint at a barbecue in Philly",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1562974669/artistImages/opaf0jhtolkj9usywqoh.jpg",
     genre: "Hip Hop"
  )
  artist_four = ActiveArtist.create!(
     new_artist_name: "Alicia Keys",
     new_song_title: "Diary",
     new_song_executive_producer: "Sony",
     new_song_youtube_url: "https://www.youtube.com/watch?v=PIksbyVq5jA",
     new_song_published_by: "Jive",
     new_artist_manager: "Clive Davis",
     new_artist_label: "Jive",
     new_song_produced_by: "Alicia Keys",
     new_song_written_by: "Alicia Keys",
     inspiration: "Song reminds me of a young lady I dated when I lived in Philly",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1562974702/artistImages/kcopyozotxvcujl1m7ro.jpg",
     genre: "R&B"
  )
  artist_five = ActiveArtist.create!(
     new_artist_name: "Tupac Sukur",
     new_song_title: "How do you want it",
     new_song_executive_producer: "Dr Dre",
     new_song_youtube_url: "https://www.youtube.com/watch?v=5jQFAP9SsPc",
     new_song_published_by: "Jive",
     new_artist_manager: "Dog Pound",
     new_artist_label: "Death Row",
     new_song_produced_by: "Dr. Dre",
     new_song_written_by: "Tupac",
     inspiration: "Hot summer in Harlem",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1550705876/artistImages/mfdfkspjrj2ytgocmwsy.jpg",
     genre: "Hip Hop"
  )
  artist_six = ActiveArtist.create!(
     new_artist_name: "Rakim",
     new_song_title: "When I be on the mic",
     new_song_executive_producer: "Clark Kent",
     new_song_youtube_url: "https://www.youtube.com/watch?v=M34OelgSlKI",
     new_song_published_by: "Universal Records",
     new_artist_manager: "Q Tip",
     new_artist_label: "Universal Records",
     new_song_produced_by: "Clark Kent",
     new_song_written_by: "Rakim",
     inspiration: "First heard this when I was living in Mt Vernon NY",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1562976257/artistImages/h9grvd6yqx5xmbrzohzv.jpg",
     genre: "Hip Hop"
  )
  artist_three.active_samples.create!(
    sampled_artist_name: "James Brown",
    sampled_song_title: "Sex Machine",
    sampled_song_executive_producer: "James Brown",
    sampled_song_published_by: "Kin Records",
    sampled_artist_manager: "Feddy Luis",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=8mjQ1i5V7qA",
    sampled_artist_label: "King",
    sampled_song_produced_by: "James Brown",
    s_inspiration: "My pops use to pump this shit",
    s_genre: "Funk",
    sampled_song_written_by: "James Brown",
    active_artist_id: artist_three.id
  )
  artist_three.active_samples.create!(
    sampled_artist_name: "Anita Baker",
    sampled_song_title: "This Love",
    sampled_song_executive_producer: "Mark Pits",
    sampled_song_published_by: "Greate Music Publishing",
    sampled_artist_manager: "Steve Stout",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=5Y9DYGiDL1o",
    sampled_artist_label: "Jive",
    sampled_song_produced_by: "Clive Davis",
    s_inspiration: "My mother user to play this all the time",
    s_genre: "R&B",
    sampled_song_written_by: "Anita Baker",
    active_artist_id: artist_three.id
  )

  artist_two.active_samples.create!(
    sampled_artist_name: "George Clinton",
    sampled_song_title: "Atomic Dog",
    sampled_song_executive_producer: "Brandy Lee",
    sampled_song_published_by: "Fisher Publishing",
    sampled_artist_manager: "Benny Boom",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=LuyS9M8T03A",
    sampled_artist_label: "Sony Records",
    sampled_song_produced_by: "George Clinton",
    s_inspiration: "This was my pops joint",
    s_genre: "Funk",
    sampled_song_written_by: "George Clinton",
    active_artist_id: artist_two.id
  )
  artist_two.active_samples.create!(
    sampled_artist_name: "Miles Davis",
    sampled_song_title: "Freddie Free Loader",
    sampled_song_executive_producer: "Miles Davis",
    sampled_song_published_by: "Big Music Group",
    sampled_artist_manager: "Jeffery ",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=zqNTltOGh5c",
    sampled_artist_label: "Columbia",
    sampled_song_produced_by: "Miles Davis",
    s_inspiration: "Some thing I listen to whenever I want to relax",
    s_genre: "Jazz",
    sampled_song_written_by: "Miles Davis",
    active_artist_id: artist_two.id
  )
  artist_four.active_samples.create!(
    sampled_artist_name: "Earth Wind and Fire",
    sampled_song_title: "After The Love Has Gone",
    sampled_song_executive_producer: "Quincy Jones",
    sampled_song_published_by: "Warner Bros",
    sampled_artist_manager: "Paul Hims",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=Gs069dndIYk",
    sampled_artist_label: "Warner",
    sampled_song_produced_by: "Mins Publishing",
    s_inspiration: "This was the joint",
    s_genre: "R&B Soul",
    sampled_song_written_by: "Eath Wind and Fire",
    active_artist_id: artist_four.id
  )
  artist_four.active_samples.create!(
    sampled_artist_name: "Aretha Franklyn",
    sampled_song_title: "Respect",
    sampled_song_executive_producer: "Tempest Beveryly",
    sampled_song_published_by: "Greater Music Group",
    sampled_artist_manager: "Quincy Jones",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=6FOUqQt3Kg0",
    sampled_artist_label: "Atlantic",
    sampled_song_produced_by: "Jerry Wexler",
    s_inspiration: "My mother love this song",
    s_genre: "R&B",
    sampled_song_written_by: "Aretha Franklyn",
    active_artist_id: artist_four.id
  )
  artist.active_samples.create!(
    sampled_artist_name: "George Clinton Parliment",
    sampled_song_title: "Give Up te Funk",
    sampled_song_executive_producer: "George Clinton",
    sampled_song_published_by: "Jim Wexly",
    sampled_artist_manager: "Ben Spits",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=Fm-ifzvOjD4",
    sampled_artist_label: "P. Funk",
    sampled_song_produced_by: "Sams Publishing",
    s_inspiration: "Just a great song",
    s_genre: "Funk",
    sampled_song_written_by: "George Clinton",
    active_artist_id: artist.id
  )
  artist.active_samples.create!(
    sampled_artist_name: "Jimmy Hendrix",
    sampled_song_title: "The wind cries Mary",
    sampled_song_executive_producer: "Melvin Davis",
    sampled_song_published_by: "Sony Music Publishing",
    sampled_artist_manager: "Tom Spades",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=ATDEtzAcTg0",
    sampled_artist_label: "Warner Music Group",
    sampled_song_produced_by: "Jimmy Hendrix",
    s_inspiration: "Great ar what he does",
    s_genre: "Funk",
    sampled_song_written_by: "Jimmy Hendrix",
    active_artist_id: artist.id
  )
  artist_five.active_samples.create!(
    sampled_artist_name: "E-40",
    sampled_song_title: "Cali",
    sampled_song_executive_producer: "Jermaine Dupree",
    sampled_song_published_by: "Grat Music",
    sampled_artist_manager: "Rush",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=0ZB9neERVfI",
    sampled_artist_label: "Death Row",
    sampled_song_produced_by: "Dr Dre",
    s_inspiration: "90's Hip Hop was incredible",
    s_genre: "Hip Hop",
    sampled_song_written_by: "E-40",
    active_artist_id: artist_five.id
  )
  artist_five.active_samples.create!(
    sampled_artist_name: "Bliss N Eso",
    sampled_song_title: "The Great Escape",
    sampled_song_executive_producer: "Esoterik",
    sampled_song_published_by: "Hot Music",
    sampled_artist_manager: "Gary Bent",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=0ZB9neERVfI",
    sampled_artist_label: "Arista",
    sampled_song_produced_by: "Bliss N Eso",
    s_inspiration: "Great sound",
    s_genre: "funk",
    sampled_song_written_by: "Bliss",
    active_artist_id: artist_five.id
  )





  pending = PendingArtist.create!(
     new_artist_name: "Tupac Sukur",
     new_song_title: "Dear Momma",
     new_song_executive_producer: "Arista Records",
     new_song_youtube_url: "https://www.youtube.com/watch?v=Mb1ZvUDvLDY",
     new_song_published_by: "Jive",
     new_artist_manager: "Benny Boom",
     new_artist_label: "Death Row",
     new_song_produced_by: "Dr. Dre",
     new_song_written_by: "Tupac",
     inspiration: "Hot summer in Harlem",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1555797532/artistImages/sfdikaazuwlwjpenm0je.png",
     genre: "Hip Hop"
  )

  pending.pending_samples.create!(
    sampled_artist_name: "George Clinton Parliment",
    sampled_song_title: "Give Up te Funk",
    sampled_song_executive_producer: "George Clinton",
    sampled_song_published_by: "Jim Wexly",
    sampled_artist_manager: "Ben Spits",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=Fm-ifzvOjD4",
    sampled_artist_label: "P. Funk",
    sampled_song_produced_by: "Sams Publishing",
    s_inspiration: "Just a great song",
    s_genre: "Funk",
    sampled_song_written_by: "George Clinton",
    pending_artist_id: pending.id
  )
  pending.pending_samples.create!(
    sampled_artist_name: "Jimmy Hendrix",
    sampled_song_title: "The wind cries Mary",
    sampled_song_executive_producer: "Melvin Davis",
    sampled_song_published_by: "Sony Music Publishing",
    sampled_artist_manager: "Tom Spades",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=ATDEtzAcTg0",
    sampled_artist_label: "Warner Music Group",
    sampled_song_produced_by: "Jimmy Hendrix",
    s_inspiration: "Great ar what he does",
    s_genre: "Funk",
    sampled_song_written_by: "Jimmy Hendrix",
    pending_artist_id: pending.id
  )

  pending_two = PendingArtist.create!(
     new_artist_name: "Big Daddy Kane",
     new_song_title: "Long Live The Kane",
     new_song_executive_producer: "Marley Marl",
     new_song_youtube_url: "https://www.youtube.com/watch?v=Mb1ZvUDvLDY",
     new_song_published_by: "Jive",
     new_artist_manager: "Klark Kent",
     new_artist_label: "Cold Chillin",
     new_song_produced_by: "Marley Marl",
     new_song_written_by: "Big Daddy Kane",
     inspiration: "I first heard this join as a young kid growing up in Brooklyn",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1555981440/artistImages/ljd3fbymjbqwcbj3ita6.jpg",
     genre: "Hip Hop"
  )
  pending_two.pending_samples.create!(
    sampled_artist_name: "George Clinton",
    sampled_song_title: "Atomic Dog",
    sampled_song_executive_producer: "Brandy Lee",
    sampled_song_published_by: "Fisher Publishing",
    sampled_artist_manager: "Benny Boom",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=LuyS9M8T03A",
    sampled_artist_label: "Sony Records",
    sampled_song_produced_by: "George Clinton",
    s_inspiration: "This was my pops joint",
    s_genre: "Funk",
    sampled_song_written_by: "George Clinton",
    pending_artist_id: pending_two.id
  )
  pending_two.pending_samples.create!(
    sampled_artist_name: "Miles Davis",
    sampled_song_title: "Freddie Free Loader",
    sampled_song_executive_producer: "Miles Davis",
    sampled_song_published_by: "Big Music Group",
    sampled_artist_manager: "Jeffery ",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=zqNTltOGh5c",
    sampled_artist_label: "Columbia",
    sampled_song_produced_by: "Miles Davis",
    s_inspiration: "Some thing I listen to whenever I want to relax",
    s_genre: "Jazz",
    sampled_song_written_by: "Miles Davis",
    pending_artist_id: pending_two.id
  )
  pending_three = PendingArtist.create!(
     new_artist_name: "Beyonce",
     new_song_title: "Love on top",
     new_song_executive_producer: "Pharell",
     new_song_youtube_url: "https://www.youtube.com/watch?v=Ob7vObnFUJc",
     new_song_published_by: "Sony",
     new_artist_manager: "Benny Boom",
     new_artist_label: "Sony",
     new_song_produced_by: "Beyonce",
     new_song_written_by: "Beyonce",
     inspiration: "First heard this joint at a barbecue in Philly",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1554348329/artistImages/g2kpmfh0ka10ng65egej.jpg",
     genre: "Hip Hop"
  )

  pending_three.pending_samples.create!(
    sampled_artist_name: "James Brown",
    sampled_song_title: "Sex Machine",
    sampled_song_executive_producer: "James Brown",
    sampled_song_published_by: "Kin Records",
    sampled_artist_manager: "Feddy Luis",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=8mjQ1i5V7qA",
    sampled_artist_label: "King",
    sampled_song_produced_by: "James Brown",
    s_inspiration: "My pops use to pump this shit",
    s_genre: "Funk",
    sampled_song_written_by: "James Brown",
    pending_artist_id: pending_three.id
  )
  pending_three.pending_samples.create!(
    sampled_artist_name: "Anita Baker",
    sampled_song_title: "This Love",
    sampled_song_executive_producer: "Mark Pits",
    sampled_song_published_by: "Greate Music Publishing",
    sampled_artist_manager: "Steve Stout",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=5Y9DYGiDL1o",
    sampled_artist_label: "Jive",
    sampled_song_produced_by: "Clive Davis",
    s_inspiration: "My mother user to play this all the time",
    s_genre: "R&B",
    sampled_song_written_by: "Anita Baker",
    pending_artist_id: pending_three.id
  )

  pending_four = PendingArtist.create!(
     new_artist_name: "Alicia Keys",
     new_song_title: "Diary",
     new_song_executive_producer: "Sony",
     new_song_youtube_url: "https://www.youtube.com/watch?v=PIksbyVq5jA",
     new_song_published_by: "Jive",
     new_artist_manager: "Clive Davis",
     new_artist_label: "Jive",
     new_song_produced_by: "Alicia Keys",
     new_song_written_by: "Alicia Keys",
     inspiration: "Song reminds me of a young lady I dated when I lived in Philly",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1555800373/artistImages/taryurywmcsyynnffco6.jpg",
     genre: "R&B"
  )

  pending_four.pending_samples.create!(
    sampled_artist_name: "Earth Wind and Fire",
    sampled_song_title: "After The Love Has Gone",
    sampled_song_executive_producer: "Quincy Jones",
    sampled_song_published_by: "Warner Bros",
    sampled_artist_manager: "Paul Hims",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=Gs069dndIYk",
    sampled_artist_label: "Warner",
    sampled_song_produced_by: "Mins Publishing",
    s_inspiration: "This was the joint",
    s_genre: "R&B Soul",
    sampled_song_written_by: "Eath Wind and Fire",
    pending_artist_id: pending_four.id
  )
  pending_four.pending_samples.create!(
    sampled_artist_name: "Aretha Franklyn",
    sampled_song_title: "Respect",
    sampled_song_executive_producer: "Tempest Beveryly",
    sampled_song_published_by: "Greater Music Group",
    sampled_artist_manager: "Quincy Jones",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=6FOUqQt3Kg0",
    sampled_artist_label: "Atlantic",
    sampled_song_produced_by: "Jerry Wexler",
    s_inspiration: "My mother love this song",
    s_genre: "R&B",
    sampled_song_written_by: "Aretha Franklyn",
    pending_artist_id: pending_four.id
  )
  pending_five = PendingArtist.create!(
     new_artist_name: "Tupac Sukur",
     new_song_title: "How do you want it",
     new_song_executive_producer: "Dr Dre",
     new_song_youtube_url: "https://www.youtube.com/watch?v=5jQFAP9SsPc",
     new_song_published_by: "Jive",
     new_artist_manager: "Dog Pound",
     new_artist_label: "Death Row",
     new_song_produced_by: "Dr. Dre",
     new_song_written_by: "Tupac",
     inspiration: "Hot summer in Harlem",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1550705876/artistImages/mfdfkspjrj2ytgocmwsy.jpg",
     genre: "Hip Hop"
  )

  pending_five.pending_samples.create!(
    sampled_artist_name: "E-40",
    sampled_song_title: "Cali",
    sampled_song_executive_producer: "Jermaine Dupree",
    sampled_song_published_by: "Grat Music",
    sampled_artist_manager: "Rush",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=0ZB9neERVfI",
    sampled_artist_label: "Death Row",
    sampled_song_produced_by: "Dr Dre",
    s_inspiration: "90's Hip Hop was incredible",
    s_genre: "Hip Hop",
    sampled_song_written_by: "E-40",
    pending_artist_id: pending_five.id
  )
  pending_five.pending_samples.create!(
    sampled_artist_name: "Bliss N Eso",
    sampled_song_title: "The Great Escape",
    sampled_song_executive_producer: "Esoterik",
    sampled_song_published_by: "Hot Music",
    sampled_artist_manager: "Gary Bent",
    sampled_song_youtube_url: "https://www.youtube.com/watch?v=0ZB9neERVfI",
    sampled_artist_label: "Arista",
    sampled_song_produced_by: "Bliss N Eso",
    s_inspiration: "Great sound",
    s_genre: "Funk",
    sampled_song_written_by: "Bliss",
    pending_artist_id: pending_five.id
  )

  pending_six = PendingArtist.create!(
     new_artist_name: "Rakim",
     new_song_title: "When I be on the mic",
     new_song_executive_producer: "Clark Kent",
     new_song_youtube_url: "https://www.youtube.com/watch?v=M34OelgSlKI",
     new_song_published_by: "Universal Records",
     new_artist_manager: "Q Tip",
     new_artist_label: "Universal Records",
     new_song_produced_by: "Clark Kent",
     new_song_written_by: "Rakim",
     inspiration: "First heard this when I was living in Mt Vernon NY",
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1555896811/artistImages/yum8edjg5owimz8uvw2y.jpg",
     genre: "Hip Hop"
  )

ArtistDatum.create!(user_id: user.id, active_artist_id: artist.id, pending_artist_id: pending.id)
ArtistDatum.create!(user_id: user_two.id, active_artist_id: artist_two.id, pending_artist_id: pending_two.id)
ArtistDatum.create!(user_id: user_three.id, active_artist_id: artist_three.id, pending_artist_id: pending_three.id)
ArtistDatum.create!(user_id: user_four.id, active_artist_id: artist_four.id, pending_artist_id: pending_four.id)
ArtistDatum.create!(user_id: user_five.id, active_artist_id: artist_five.id, pending_artist_id: pending_five.id)

    client = user.clients.create(
      label: "Art@War",
      distributer: "Atlantic Records",
      contact_name: "Tim Brown",
      company: "879 Eighth Avenue, NYC 10012",
      signer: "Tim Brown",
      payee: "James Brown",
      email: "james@example.com",
      phone_number: "908-234-5687",
      clearance_deadline: "01-11-2019",
    )


  client.submissions.create(
         artist_name: "June The Moon",
         song_name: "Exibit C",
         song_link: "https://www.youtube.com/watch?v=-lrVO3pPDP4",
         writers: "John Davis",
         publishers:  "John Davis Publishing",
         splits:  "50/50",
         sampled_song:  "Cross My Heart",
         link_sampled_song: "https://www.youtube.com/watch?v=mY4AGgH6swc",
         sampled_song_label: "Chess Records",
         writers_sampled_song: "Billy Stuart",
         sampled_publishing:   "Billy's Songs",
         sample_description:   "Four bar vocals",
         master:           "Yes",
         publishing_sample_used: "Yes"
      )

  client.submissions.create(
         artist_name: "Michael Stuart",
         song_name: "That Girl",
         song_link: "https://www.youtube.com/watch?v=-lrVO3pPDP4",
         writers: "Michael Stuart",
         publishers:  "Davis Publishing",
         splits:  "50/50",
         sampled_song:  "The Girl is min",
         link_sampled_song: "https://www.youtube.com/watch?v=mY4AGgH6swc",
         sampled_song_label: "Warner Records",
         writers_sampled_song: "Paul McCarthy/Michael Jackson",
         sampled_publishing:   "Jackson Publishing",
         sample_description:   "8 bar vocals",
         master:           "Yes",
         publishing_sample_used: "Yes",
      )




    client_two = user_two.clients.create(
      label: "Imperial",
      distributer: "Sony",
      contact_name: "Ben Carson",
      company: "768 Eighth Avenue, NY NY 10025",
      signer: "Ben Carson",
      payee: "Ben Carson",
      email: "ben@example.com",
      phone_number: "904-342-7812",
      clearance_deadline: "03-12-2019"
    )


  client_two.submissions.create(
         artist_name: "Sam Davis",
         song_name: "Drifting",
         song_link: "https://www.youtube.com/watch?v=-lrVO3pPDP4",
         writers: "Sam Davis",
         publishers:  "Sam Davis Publishing",
         splits:  "50/50",
         sampled_song:  "Not gone let",
         link_sampled_song: "https://www.youtube.com/watch?v=mY4AGgH6swc",
         sampled_song_label: "Good Music",
         writers_sampled_song: "Greg Spittin",
         sampled_publishing:   "Spittin Publishing",
         sample_description:   "Four bar vocals",
         master:           "Yes",
         publishing_sample_used: "Yes"
      )

  client_two.submissions.create(
         artist_name: "Sam Davis",
         song_name: "It takes two",
         song_link: "https://www.youtube.com/watch?v=-lrVO3pPDP4",
         writers: "Sam Davis",
         publishers:  "Davis Publishing",
         splits:  "50/50",
         sampled_song:  "Not tonight",
         link_sampled_song: "https://www.youtube.com/watch?v=mY4AGgH6swc",
         sampled_song_label: "Cold Chillin",
         writers_sampled_song: "Colonel Abrams",
         sampled_publishing:   "Abram Publishing",
         sample_description:   "8 bar vocals",
         master:           "Yes",
         publishing_sample_used: "Yes"
      )


   user.audio_uploads.create(
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1562975843/artistImages/wcvsaoys6tcu2kvltsed.jpg",
		 audio_url: "https://res.cloudinary.com/dtfslc8sx/video/upload/v1572799152/Lullaby.mp3",
		 name: "Michael Jackson",
		 title: "Missing You",
		 trackbio: "Michael Jackson just inspires me",
		 genre: "Pop",
		 artistbio: "Im originally from NY. My was father was a DJ hence my love for music"
	 )
   user_two.audio_uploads.create(
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1555981440/artistImages/ljd3fbymjbqwcbj3ita6.jpg",
		 audio_url: "https://res.cloudinary.com/dtfslc8sx/video/upload/v1572799138/05_Juicy_Fruit.mp3",
		 name: "Marvin Gaye",
		 title: "Sexual Healing",
		 trackbio: "I love how Marvin Gaye just captures the times",
		 genre: "R&B",
		 artistbio: "Im originally from NY. My was father was a DJ hence my love for music"
	 )
   user_three.audio_uploads.create(
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1562974702/artistImages/kcopyozotxvcujl1m7ro.jpg",
		 audio_url: "https://res.cloudinary.com/dtfslc8sx/video/upload/v1572799152/13_Lyrics_To_Go.mp3",
		 name: "Diana Ross",
		 title: "So excited",
		 trackbio: "Diana is the queen I had to add my touch",
		 genre: "Pop",
		 artistbio: "Im originally from NY. My was father was a DJ hence my love for music"
	 )
   user_four.audio_uploads.create(
     image_url: "https://res.cloudinary.com/dtfslc8sx/image/upload/v1550705876/artistImages/mfdfkspjrj2ytgocmwsy.jpg",
		 audio_url: "https://res.cloudinary.com/dtfslc8sx/video/upload/v1572799112/02_I_Wanna_Be_Where_You_Are.mp3",
		 name: "Lionell Richie",
		 title: "Dancing on the ceiling",
		 trackbio: "First time I heard this it played on Hot Tracks",
		 genre: "Pop",
		 artistbio: "Im originally from NY. My was father was a DJ hence my love for music"
	 )

