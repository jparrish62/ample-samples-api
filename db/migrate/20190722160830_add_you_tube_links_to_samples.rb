class AddYouTubeLinksToSamples < ActiveRecord::Migration[5.2]
  def change
    add_column :samples, :youtube_links, :string
  end
end
