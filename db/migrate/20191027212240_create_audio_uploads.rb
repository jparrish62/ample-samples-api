class CreateAudioUploads < ActiveRecord::Migration[5.2]
  def change
    create_table :audio_uploads do |t|
      t.string :image_url
      t.string :audio_url
      t.string :name
      t.string :title
      t.text :trackbio
      t.string :genre
      t.string :artistbio
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
