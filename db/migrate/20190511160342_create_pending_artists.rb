class CreatePendingArtists < ActiveRecord::Migration[5.2]
  def change
    create_table :pending_artists do |t|
      t.string :new_artist_name
      t.string :new_song_title
      t.string :new_song_executive_producer
      t.string :new_song_youtube_url
      t.string :new_song_published_by
      t.string :new_artist_manager
      t.string :new_artist_label
      t.string :new_song_produced_by
      t.string :new_song_written_by
      t.string :inspiration
      t.string :image_url
      t.string :genre
      t.string :genre
      t.integer :active_ref_id

      t.timestamps
    end
  end
end
