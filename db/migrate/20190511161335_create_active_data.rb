class CreateActiveData < ActiveRecord::Migration[5.2]
  def change
    create_table :active_data do |t|
      t.references :user, foreign_key: true
      t.references :active_artist, foreign_key: true
      t.references :pending_artist, foreign_key: true

      t.timestamps
    end
  end
end
