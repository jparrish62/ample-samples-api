class CreateSubmissions < ActiveRecord::Migration[5.2]
  def change
    create_table :submissions do |t|
      t.string :artist_name
      t.string :song_name
      t.string :song_link
      t.string :writers
      t.string :publishers
      t.string :splits
      t.string :sampled_song
      t.string :link_sampled_song
      t.string :sampled_song_label
      t.string :writers_sampled_song
      t.string :sampled_publishing
      t.string :sample_description
      t.string :master
      t.string :publishing_sample_used
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
