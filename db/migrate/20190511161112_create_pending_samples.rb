class CreatePendingSamples < ActiveRecord::Migration[5.2]
  def change
    create_table :pending_samples do |t|
      t.string :sampled_artist_name
      t.string :sampled_song_title
      t.string :sampled_song_executive_producer
      t.string :sampled_song_published_by
      t.string :sampled_artist_manager
      t.string :sampled_song_youtube_url
      t.string :sampled_artist_label
      t.string :sampled_song_produced_by
      t.text :s_inspiration
      t.string :s_genre
      t.string :sampled_song_written_by
      t.references :pending_artist, foreign_key: true

      t.timestamps
    end
  end
end
