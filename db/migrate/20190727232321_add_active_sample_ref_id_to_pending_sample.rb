class AddActiveSampleRefIdToPendingSample < ActiveRecord::Migration[5.2]
  def change
    add_column :pending_samples, :active_sample_ref_id, :integer
  end
end
