class CreateAudios < ActiveRecord::Migration[5.2]
  def change
    create_table :audios do |t|
      t.string :label
      t.string :project
      t.string :contact
      t.string :payment
      t.string :email
      t.string :company
      t.string :signature
      t.string :clearance_date

      t.timestamps
    end
  end
end
