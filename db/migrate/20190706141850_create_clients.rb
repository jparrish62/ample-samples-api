class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :label
      t.string :distributer
      t.string :contact_name
      t.string :company
      t.string :signer
      t.string :payee
      t.string :email
      t.string :phone_number
      t.string :clearance_deadline
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
