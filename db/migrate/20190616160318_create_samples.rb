class CreateSamples < ActiveRecord::Migration[5.2]
  def change
    create_table :samples do |t|
      t.string :artist
      t.string :song_title
      t.text :song_samples
      t.text :youtub_links

      t.timestamps
    end
  end
end
