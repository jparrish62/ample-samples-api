class CreateStripePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :stripe_payments do |t|
      t.string :token_id
      t.string :card_id
      t.string :city
      t.string :address_country
      t.string :address
      t.string :address_check
      t.string :state
      t.string :zip
      t.string :zip_check
      t.string :brand
      t.string :country
      t.string :cvc_check
      t.string :name
      t.integer :ex_month
      t.integer :exp_year
      t.string :funding
      t.string :last4
      t.string :ip
      t.string :email
      t.string :type
      t.string :billing_name
      t.string :billing_country_code
      t.string :billing_zip
      t.string :billing_address
      t.string :billing_city
      t.string :billing_state
      t.string :shipping_name
      t.string :shipping_address_country
      t.string :shipping_address_country_code
      t.string :shipping_zip
      t.string :shipping_address
      t.string :shipping_city
      t.string :shipping_city

      t.timestamps
    end
  end
end
