# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_15_023544) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_artists", force: :cascade do |t|
    t.string "new_artist_name"
    t.string "new_song_title"
    t.string "new_song_executive_producer"
    t.string "new_song_youtube_url"
    t.string "new_song_published_by"
    t.string "new_artist_manager"
    t.string "new_artist_label"
    t.string "new_song_produced_by"
    t.string "new_song_written_by"
    t.string "inspiration"
    t.string "image_url"
    t.string "genre"
    t.integer "pending_ref_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "active_data", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "active_artist_id"
    t.bigint "pending_artist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["active_artist_id"], name: "index_active_data_on_active_artist_id"
    t.index ["pending_artist_id"], name: "index_active_data_on_pending_artist_id"
    t.index ["user_id"], name: "index_active_data_on_user_id"
  end

  create_table "active_samples", force: :cascade do |t|
    t.string "sampled_artist_name"
    t.string "sampled_song_title"
    t.string "sampled_song_executive_producer"
    t.string "sampled_song_published_by"
    t.string "sampled_artist_manager"
    t.string "sampled_song_youtube_url"
    t.string "sampled_artist_label"
    t.string "sampled_song_produced_by"
    t.text "s_inspiration"
    t.string "s_genre"
    t.string "sampled_song_written_by"
    t.bigint "active_artist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["active_artist_id"], name: "index_active_samples_on_active_artist_id"
  end

  create_table "artist_data", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "active_artist_id"
    t.bigint "pending_artist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["active_artist_id"], name: "index_artist_data_on_active_artist_id"
    t.index ["pending_artist_id"], name: "index_artist_data_on_pending_artist_id"
    t.index ["user_id"], name: "index_artist_data_on_user_id"
  end

  create_table "audio_uploads", force: :cascade do |t|
    t.string "image_url"
    t.string "audio_url"
    t.string "name"
    t.string "title"
    t.text "trackbio"
    t.string "genre"
    t.string "artistbio"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_audio_uploads_on_user_id"
  end

  create_table "audios", force: :cascade do |t|
    t.string "label"
    t.string "project"
    t.string "contact"
    t.string "payment"
    t.string "email"
    t.string "company"
    t.string "signature"
    t.string "clearance_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string "label"
    t.string "distributer"
    t.string "contact_name"
    t.string "company"
    t.string "signer"
    t.string "payee"
    t.string "email"
    t.string "phone_number"
    t.string "clearance_deadline"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_clients_on_user_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "message"
    t.string "phone_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pending_artists", force: :cascade do |t|
    t.string "new_artist_name"
    t.string "new_song_title"
    t.string "new_song_executive_producer"
    t.string "new_song_youtube_url"
    t.string "new_song_published_by"
    t.string "new_artist_manager"
    t.string "new_artist_label"
    t.string "new_song_produced_by"
    t.string "new_song_written_by"
    t.string "inspiration"
    t.string "image_url"
    t.string "genre"
    t.integer "active_ref_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pending_samples", force: :cascade do |t|
    t.string "sampled_artist_name"
    t.string "sampled_song_title"
    t.string "sampled_song_executive_producer"
    t.string "sampled_song_published_by"
    t.string "sampled_artist_manager"
    t.string "sampled_song_youtube_url"
    t.string "sampled_artist_label"
    t.string "sampled_song_produced_by"
    t.text "s_inspiration"
    t.string "s_genre"
    t.string "sampled_song_written_by"
    t.bigint "pending_artist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "active_sample_ref_id"
    t.index ["pending_artist_id"], name: "index_pending_samples_on_pending_artist_id"
  end

  create_table "samples", force: :cascade do |t|
    t.string "artist"
    t.string "song_title"
    t.text "song_samples"
    t.text "youtub_links"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "youtube_links"
  end

  create_table "stripe_payments", force: :cascade do |t|
    t.string "token_id"
    t.string "card_id"
    t.string "city"
    t.string "address_country"
    t.string "address"
    t.string "address_check"
    t.string "state"
    t.string "zip"
    t.string "zip_check"
    t.string "brand"
    t.string "country"
    t.string "cvc_check"
    t.string "name"
    t.integer "ex_month"
    t.integer "exp_year"
    t.string "funding"
    t.string "last4"
    t.string "ip"
    t.string "email"
    t.string "type"
    t.string "billing_name"
    t.string "billing_country_code"
    t.string "billing_zip"
    t.string "billing_address"
    t.string "billing_city"
    t.string "billing_state"
    t.string "shipping_name"
    t.string "shipping_address_country"
    t.string "shipping_address_country_code"
    t.string "shipping_zip"
    t.string "shipping_address"
    t.string "shipping_city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "submissions", force: :cascade do |t|
    t.string "artist_name"
    t.string "song_name"
    t.string "song_link"
    t.string "writers"
    t.string "publishers"
    t.string "splits"
    t.string "sampled_song"
    t.string "link_sampled_song"
    t.string "sampled_song_label"
    t.string "writers_sampled_song"
    t.string "sampled_publishing"
    t.string "sample_description"
    t.string "master"
    t.string "publishing_sample_used"
    t.bigint "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_submissions_on_client_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "nickname"
    t.string "password_digest"
    t.string "auth_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role"
  end

  add_foreign_key "active_data", "active_artists"
  add_foreign_key "active_data", "pending_artists"
  add_foreign_key "active_data", "users"
  add_foreign_key "active_samples", "active_artists"
  add_foreign_key "artist_data", "active_artists"
  add_foreign_key "artist_data", "pending_artists"
  add_foreign_key "artist_data", "users"
  add_foreign_key "audio_uploads", "users"
  add_foreign_key "clients", "users"
  add_foreign_key "pending_samples", "pending_artists"
  add_foreign_key "submissions", "clients"
end
