Rails.application.routes.draw do
 namespace :api, defaults: { format: :json } do
    scope module: :v1 do
      resources :active_artist,   only: [:update, :create, :show, :index]
			resources :stripe_payments, only: [:create]
			resources :audio_uploads,   only: [:create, :show, :index, :update, :destroy]
      resources :active_samples,  only: [:update, :create, :show, :index]
      resources :pending_samples, only: [:update, :destroy]
      resources :tokens,          only: [:create, :destroy]
      resources :clients,         only: [:index,  :show]
      resources :contact_us,      only: [:create]
      resources :licenses,        only: [:create]
      resources :pending_artist
      post      '/approve'        => 'pending_artist#approve'
      post      '/approve_all'    => 'pending_artist#approve_all'
      get       '/fetch_samples'  => 'sample_scrapers#fetch_samples'
      get       '/fetch_links'    => 'sample_scrapers#fetch_links'
    end
  end
end
