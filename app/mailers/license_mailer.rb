class LicenseMailer < ApplicationMailer
  def request_license
    @client = params[:client].to_h
    @submission = params[:submission]
    mail(to: 'theamplesamples@gmail.com', subject: 'Ample Samples User')
  end
end
