class ApplicationMailer < ActionMailer::Base
  default from: 'theamplesamples@gmail.com'
  layout 'mailer'
end
