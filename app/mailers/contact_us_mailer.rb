class ContactUsMailer < ApplicationMailer
  def contact_us
    @user = params[:email]
    mail(to: 'theamplesamples@gmail.com', subject: 'Ample Samples User')
  end
end
