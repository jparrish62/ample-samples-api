class ActiveArtist < ApplicationRecord
  has_many :active_samples
  has_many :users, through: :artist_data

  def self.create_active_artist_from_pending(pending)
    create_active_artist(t_hash(pending), pending.id)
  end

  def self.create_active_artist(data, from_pending_id=nil)
		active = ActiveArtist.create(t_hash(data).except('active_ref_id', 'id'))
    active.pending_ref_id = from_pending_id
    active.save!
    active
  end

  def self.active_artist(id)
    ActiveArtist.find(id)
  end

  def self.active_index
    ActiveArtist.all
  end

  def self.set_pending_ref_id(active, pending)
    active.pending_ref_id = pending.id
    active.save
  end

  def self.remove_pending_ref_id(id)
    active = ActiveArtist.find(id)
    active.pending_ref_id = nil
    active.save
  end

	private 

	def self.t_hash(data)
		if data.class == Hash
			data 
		else
			data.attributes.to_h
		end
	end
end
