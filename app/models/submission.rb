class Submission < ApplicationRecord
  belongs_to :client

  def self.create_submission(client, submission)
    submission.each do |sub|
      client.submissions.create(
         artist_name: sub[:artist_name],
         song_name: sub[:song_name],
         song_link: sub[:song_link],
         writers:  sub[:writers],
         publishers:  sub[:publishers],
         splits:  sub[:splits],
         sampled_song:   sub[:sampled_song],
         link_sampled_song:  sub[:link_sampled_song],
         sampled_song_label:  sub[:sampled_song_label],
         writers_sampled_song:  sub[:writers_sampled_song],
         sampled_publishing:   sub[:sampled_publishing],
         sample_description:   sub[:sample_description],
         master:           sub[:master],
         publishing_sample_used: sub[:publishing_sample_used]
      )
    end
  end
end
