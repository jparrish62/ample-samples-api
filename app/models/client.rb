class Client < ApplicationRecord
  belongs_to :user
  has_many   :submissions

  def self.create_client(token, client)
    user = User.find_user(token)
    user.clients.create(
      label: client[:label],
      distributer: client[:distributer],
      contact_name: client[:contact_name],
      company: client[:company],
      signer: client[:signer],
      payee: client[:payee],
      email: client[:email],
      phone_number: client[:phone_number],
      clearance_deadline: client[:clearance_deadline]
    )
  end
end
