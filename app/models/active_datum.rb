class ActiveDatum < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :active_artist, optional: true
  belongs_to :pending_artist, optional: true

	def self.find_artist_datum(pending)
		ArtistDatum.find_by_pending_artist_id(pending.id)
	end
end
