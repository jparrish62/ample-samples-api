class ActiveSample < ApplicationRecord
  belongs_to :active_artist

 def self.create_active_samples(artist, sample)
	 artist.active_samples.create(t_hash(sample).except('active_sample_ref_id', 'pending_artist_id', 'id'))
 end

 def self.update_active_sample(sample)
   active_sample = ActiveSample.find(sample.active_sample_ref_id)
	 active_sample.update_attributes(t_hash(sample).except('active_sample_ref_id', 'pending_artist_id', 'id'))
 end



  def self.find_active_sample(id)
    ActiveSample.find(id)
  end

	private 

	def self.t_hash(data)
		if data.class == Hash
			data 
		else
			data.attributes.to_h
		end
	end
end
