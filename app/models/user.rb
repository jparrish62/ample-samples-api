class User < ApplicationRecord
  has_many :artist_data
  has_many :active_artist,  through: :artist_data
  has_many :pending_artist, through: :artist_data
  has_many :clients
	has_many :audio_uploads
  has_secure_password
  validates_presence_of :password_digest
  enum role: [:user, :admin]
  after_initialize :set_default_role, :if => :new_record?

  def set_default_role
    self.role ||= :user
  end

  def self.find_user(token)
    User.find_by_auth_token(token) if token.present?
  end
end
