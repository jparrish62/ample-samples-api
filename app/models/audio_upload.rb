class AudioUpload < ApplicationRecord
  belongs_to :user

	def self.create_record(current_user, data)
		current_user.audio_uploads.create(AudioUpload.attribute(data))
	end

	def self.find_record(id)
		AudioUpload.find(id)
	end
end
