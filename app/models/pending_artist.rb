class PendingArtist < ApplicationRecord

  has_many :pending_samples, :dependent => :delete_all
  has_many :users, through: :artist_data

  def self.create_pending_artist_from_active(active)
		pending = PendingArtist.create(t_hash(active).except('pending_ref_id', 'id'))
    pending.active_ref_id = active.id
    pending.save!
    pending
  end

  def self.create_pending(data, from_active_id=nil)
    pending = PendingArtist.create(data.permit!.except(:active_ref_id))
    pending.active_ref_id = from_active_id
    pending.save!
    pending
  end

  def self.add_sample_to_pending(pending_id, sample_data)
    pending = find_pending_artist(pending_id)
    pending.pending_samples.create(sample_data.permit!)
  end

  def self.update_pending(id, data)
    pending = find_pending_artist(id)
    pending.update(data.permit!)
  end

  def self.find_pending_artist(id)
    PendingArtist.find_by_id(id)
  end

  def self.add_sample(pending, data)
    sample = pending.pending_samples
    sample.create(data.permit!)
  end

  def self.destroy_pending(pending)
    pending.destroy
  end

	private 

	def self.t_hash(data)
		if data.class == Hash
			data 
		else
			data.attributes.to_h
		end
	end
end

