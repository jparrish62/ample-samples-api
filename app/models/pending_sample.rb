class PendingSample < ApplicationRecord
  belongs_to :pending_artist

  def self.find_pending_sample(id)
    PendingSample.find(id)
  end

  def self.find_by_song_title(title)
    PendingSample.find_by_sample_song_title(title)
  end

  def self.create_duplicate_pending(pending, sample, data)
    new_sample = sample.dup
    new_sample.update_attributes(data.permit!)
  end

  def self.update_sample(pending, sample, data)
		new_sample = pending.pending_samples.create(t_hash(sample).except('id', 'active_artist_id'))
    new_sample.active_sample_ref_id = sample.id
    new_sample.save!
    new_sample.update(data.permit!)
  end

  def self.destroy_sample(id)
    sample = PendingSample.find(id)
    sample.destroy
  end

	private 

	def self.t_hash(data)
		if data.class == Hash
			data 
		else
			data.attributes.to_h
		end
	end
end
