class ArtistDatum < ApplicationRecord
  belongs_to :user
  belongs_to :active_artist
  belongs_to :pending_artist

  def self.set_pending_id_to_artist_datum(active, pending)
    datum = ArtistDatum.find_by_active_artist_id(active.id)
    datum.pending_artist_id = pending.id
    datum.save
  end

  def self.set_artist_datum(user, active)
    datum = ArtistDatum.find_by_user_id(user.id)
    datum.active_artist_id = active.id
    datum.save
  end

  def self.create_artist_datum(user_id, pending_id)
    ArtistDatum.create(user_id: user_id, pending_artist_id: pending_id)
  end

  def self.remove_pending_id(id)
    datum = ArtistDatum.find_by_active_artist_id(id)
    datum.pending_artist_id = nil
    datum.save
  end

  def self.find_artist_datum_pending_id(id)
    ArtistDatum.find_by_pending_artist_id(id)
  end

  def self.find_artist_datum_active_id(id)
    ArtistDatum.find_by_active_artist_id(id)
  end
end
