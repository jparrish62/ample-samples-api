class Api::V1::AudioClearancesController < ApplicationController
  respond_to :json

  def create
    audio = Audio::AudioCreateCommand.call(params)
    if audio.save
      render json: audio, status: 201
    else
      render json: errors { errors: audio.errors }, status: 422
    end
  end

  def index
    respond_with Audio.all
  end

  def update
    audio = Audio::AudioUpdateCommand.call(params)
    if audio.update
      render json: audio, status: 201
    else
      render json: { errors: audio.errors }, status: 422
    end
  end

  def show
    respond_with Audio::AudioShowCommand.call(params)
  end
end
