class Api::V1::SampleScrapersController < ApplicationController
respond_to :json

  def fetch_samples
    respond_with Scraper::FetchSamplesCommand.call(params)
  end

  def fetch_links
    respond_with Scraper::FetchLinksCommand.call(params)
  end
end
