class Api::V1::ActiveSamplesController < ApplicationController
  respond_to :json
	before_action :current_user

  def update
    sample = ActiveSamples::ActiveSampleUpdateCommand.call(params)
    if sample
      render json: sample, status: 201
    else
      render json: { errors: sample.errors }, status: 422
    end
  end

  private
  def current_user
    CurrentUser::CurrentUserCommand.call(params)
  end
end
