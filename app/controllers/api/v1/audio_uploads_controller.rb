class Api::V1::AudioUploadsController < ApplicationController
	before_action :current_user, only: [:create]
	respond_to :json

	def create
		upload = Upload::AudioUploadsCreateCommand.call(params)
		if upload.save
			render json: upload, status: 201
		else
			render json: { errors: "Sign In" }, status: 422
	  end
	end

  def index
		respond_with AudioUpload.all 
  end

	def update
		respond_with Upload::AudioUploadsUpdateCommand.call(params)
	end

  def show
		respond_with Upload::AudioUploadsShowCommand.call(params)
  end

	def destroy
   respond_with  Upload::AudioUploadsDestroyCommand.call(params)
	end


	private 

	def current_user
		respond_with CurrentUser::CurrentUserCommand.call(params)
	end
end
