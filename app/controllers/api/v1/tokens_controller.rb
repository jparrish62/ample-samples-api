class Api::V1::TokensController < ApplicationController
  respond_to :json

  def create
    user = Token::CreateTokenCommand.call(params)
    if user
			render json: { jwt: user.auth_token, role: user.role }, status: 201
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def destroy
    Token::DestroyTokenCommand.call(params)
  end
end
