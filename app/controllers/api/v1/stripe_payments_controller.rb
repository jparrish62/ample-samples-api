class Api::V1::StripePaymentsController < ApplicationController
  respond_to :json
	before_action :current_user

	def create 
		if charge.save
			DownloadAudioMailer.with(link).download_link.deliver_later
			render json: charge, status: 201
		else
			render json: { errors: charge.errors }, status: 422
	  end
	end

	private 

  def current_user
    CurrentUser::CurrentUserCommand.call(params)
  end

	def link
	  SendDownLoadLink::DownLoadLink.call(params[:audio_url])
	end

	def charge
		Stripe::StripePaymentsCreateCommand.call(params)
	end
end
