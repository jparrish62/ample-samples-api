class Api::V1::ActiveArtistController < ApplicationController
  respond_to :json
	before_action :current_user, only: [:create, :update]

  def update
    pending = Active::ActiveUpdateCommand.call(params)
    if pending
      render  json: pending , status: 201
    else
      render  json: { errors: update.errors }, status: 422
    end
  end

  def create
    artist = Active::ActiveCreateCommand.call(params)
    if artist
      render json: artist, status: 201
    else
      render json: { errors: artist.errors }, status: 422
    end
  end

  def show
    artist = Active::ActiveShowCommand.call(params)
    respond_with [artist, artist.active_samples]
  end

  def index
    respond_with Active::ActiveIndexCommand.active_index
  end

  private
  def current_user
    CurrentUser::CurrentUserCommand.call(params)
  end
end
