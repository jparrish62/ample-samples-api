class Api::V1::PendingArtistController < ApplicationController
  respond_to :json
	before_action :current_admin, only: [:update, :index]

  def create
    pending = Pending::PendingCreateCommand.call(params)
    if pending
      render json: pending, status: 201
    else
      render json: { errors: pending.errors }, status: 422
    end
  end

  def update
    pending = Pending::PendingUpdateCommand.call(params)
    if pending
      render json: pending, status: 201
    else
      render json: { errors: pending.errors }, status: 422
    end
  end

  def show
    pending = Pending::PendingShowCommand.call(params)
    respond_with [pending, pending.pending_samples]
  end

  def index
    respond_with  PendingArtist.all
  end

  def destroy
    Pending::PendingDestroyCommand.call(params)
  end

  def approve
    approved = Approved::ApprovedCommand.call(params)
    if approved
      render json: approved, status: 201
    else
      render json: { errors: approved.errors }, status: 422
    end
  end

  def approve_all
    approved = Approved::ApprovedAllCommand.call(params)
    if approved
      render json: approved, status: 201
    else
      render json: { errors: approved.errors }, status: 422
    end
  end

  private

  def current_admin
    CurrentUser::CurrentAdminCommand.call(params)
  end

  def current_user
    CurrentUser::CurrentUserCommand.call(params)
  end
end



