class Api::V1::ContactUsController < ApplicationController
  respond_to :json
  def create
   email = Contact.create(contact_us_params)
   if email.save
     ContactUsMailer.with(email: email).contact_us.deliver_later
     render  json: email, status: 201
   else
     render json:  { erros: email.errors }, status: 422
   end
  end

  private
  def contact_us_params
    params.require(:contact_us).permit(:name, :email, :phone_number, :message )
  end
end
