class Api::V1::ClientsController < ApplicationController
  respond_to :json

  def show
    client = Client.find(params[:id])
    respond_with [client, client.submissions]
  end

  def index
		respond_with Client.all
  end

  private
  def current_admin
    CurrentUser::CurrentAdminCommand.(params)
  end
end
