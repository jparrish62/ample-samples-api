class Api::V1::LicensesController < ApplicationController
  respond_to :json
	before_action :current_user

  def create
    project = License::LicenseCreateCommand.call(params)
    if project
      LicenseMailer.with(client: params[:client].permit!, submission: params[:submission].each { |sub| sub.permit!}).request_license.deliver_now
      render json: project, status: 201
    else
      render json: {errors: project.errors }, status: 442
    end
  end

  private

  def current_user
    CurrentUser::CurrentUserCommand.call(params)
  end
end
