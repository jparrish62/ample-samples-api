class Api::V1::PendingSamplesController < ApplicationController
  respond_to :json
	before_action :current_admin

  def update
    pending = PendingSamples::PendingSamplesUpdateCommand.call(params)
    if pending
      render json: pending, status: 201
    else
      render json: {errors: pending.errors }, status: 422
    end
  end

  def destroy
    PendingSamples::PendingSamplesDestroyCommand.call(params)
  end

  private
  def current_admin
    CurrentUser::CurrentAdminCommand.call(params)
  end
end
